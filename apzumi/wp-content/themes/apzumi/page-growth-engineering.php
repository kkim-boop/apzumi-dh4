<?php get_header(); ?>

<div id="growth-engineering">

    <div id="growth-hero" class="section flex slogan" <?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
        <div class="container">
            <h1>Small or big, new challenges energize & drive us to maximize your online performance.</h1>
            <h3>Your online marketing will be enhanced and highly effective with the use of the latest technology.</h3>
        </div>
        <img src="<?php echo apzumi_image_directory()?>/arrow-down.png" class="arrow-down">
    </div>

    <div id="moving-business" class="section flex">
        <div class="container">
            <div class="section-title">Moving business into digital world</div>
            <h1>
                We will find the <span class="red">perfect online location</span> for your business.
            </h1>
            <div class="flex align-center small-vertical">
                <img src="<?php echo apzumi_image_directory()?>/moving-business.png" class="no-mobile">
                <img src="<?php echo apzumi_image_directory()?>/moving-business-mobile.png" class="only-mobile">
                <h2>
                    By discovering your audience, their needs and how to connect with them, we will build your content strategy and guide your business into the Internet.<br><br>
                    We expertly create <span class="red">long-lasting impressions</span> through tools such as mobile performance marketing, lifecycle marketing and digital media planning.
                </h2>
            </div>
        </div>
    </div>

    <div id="profile-targeting" class="section flex" data-offset="-60">
        <div class="container">
            <div class="section-title">Profile targeting</div>
            <div class="description">
                <h1>
                    We ensure all your assets <span class="red">perform at their maximum</span> potential.
                </h1>
                <img src="<?php echo apzumi_image_directory()?>/profile-targeting.png" class="only-mobile">
                <h2>
                    We find your specific mobile audience with network optimization systems and allow your product to target buyers based on their conversion activity.
                </h2>
            </div>
            <img src="<?php echo apzumi_image_directory()?>/profile-targeting.png" class="no-mobile">

        </div>
    </div>

    <div id="brand-visibility" class="section flex" data-offset="0">
        <div class="container padded section-content">
            <div class="section-title">Brand visibility online</div>
            <div class="description">
                <h1>
                    With our partnership, <span class="red">you will increase</span> the <span class="red">traffic and conversion rates</span> of your website
                </h1>
                <img src="<?php echo apzumi_image_directory()?>/brand-visibility-mobile.png" class="only-mobile">
                <h2>You will atchive it by systems that support <span class="red">advertising and positioning</span>, such as ad network solutions, search optimization, product listing ads and dynamic ads.</h2>
            </div>
            <div class="img no-mobile"><img src="<?php echo apzumi_image_directory()?>/brand-visibility.png"></div>


        </div>
    </div>

    <div id="data-sync" class="section flex" data-offset="-60">
        <div class="container padded section-content">
            <div class="section-title">Data sync</div>
            <div class="container-content">
                <h1>Thanks to our consumer intelligence systems, your <span class="red">media decisions</span> are always smart and informed.</h1>
                <div class="img only-mobile"><img src="<?php echo apzumi_image_directory()?>/data-sync-mobile.png"></div>
                <div class="img no-mobile"><img src="<?php echo apzumi_image_directory()?>/data-sync.png"></div>
                <h2>We provide you with <span class="red">data on all your actions and those of your customers</span>, as all data is collected and analyzed. Our solutions integrate data from various sources and convert it into easy-to-read reports. Our system will immediately expand your <span class="red">customer interaction.</span></h2>
            </div>
        </div>
    </div>

    <a class="full-width-button button button-red no-fixed" href="/contact">
        <span>LET'S TALK</span>
        <span class="right-arrow"></span>
    </a>

</div>

<?php
get_footer();

