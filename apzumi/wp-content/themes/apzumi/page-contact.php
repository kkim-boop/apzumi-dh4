<?php get_header();?>
<div id="contact-page">

    <div id="contact-hero" class="section flex slogan"<?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
        <div class="container padded">
            <h1>We're international company with offices in the U.S. and Europe.</h1>
        </div>
        <img src="<?php echo apzumi_image_directory()?>/arrow-down.png" class="arrow-down">
    </div>

    <div id="addresses" class="section">
        <div class="address active" onclick="showUSA()">
            <img src="<?php echo apzumi_image_directory()?>/pin.png"><br>
            <h1>USA</h1>
            <span>201 West Main Street Suite 313<br>
            Durham, NC 27701
            </span>
            <img src="<?php echo apzumi_image_directory()?>/map-arrow.png" class="map-arrow">
        </div>
        <div class="address" onclick="showPoland()">
            <img src="<?php echo apzumi_image_directory()?>/pin.png"><br>
            <h1>POLAND</h1>
            <span>Skladowa 5B/11<br>
            61-888 Poznan
            </span>
            <img src="<?php echo apzumi_image_directory()?>/map-arrow.png" class="map-arrow">
        </div>
        <div class="address" onclick="showGermany()">
            <img src="<?php echo apzumi_image_directory()?>/pin.png"><br>
            <h1>GERMANY</h1>
            <span>Platz der Deutschen Einheit 2<br>
            60327 Frankfurt
            </span>
            <img src="<?php echo apzumi_image_directory()?>/map-arrow.png" class="map-arrow">
        </div>
        <div id="map_canvas"></div>
    </div>

    <div id="contact-form" class="section flex">
        <div class="container padded default-width">
            <div class="section-title">Contact</div>
            <h1>DROP US A LINE</h1>
            <h2><div>We’ll estimate your project</div>
                    <div>We’ll help you to get funding</div>
                    <div>We’ll <span class="red">make your app come true</span></div>
            </h2>
            <form method="post" action="<?php echo get_template_directory_uri(); ?>/mail/send2.php" class="contact">
                <div class="col">
                    <input type="text" placeholder="Name" name="firstName">
                    <input type="text" placeholder="Surname" name="surname">
                    <input type="text" placeholder="Company" name="company">
                </div>
                <div class="col">
                    <input type="text" placeholder="E-mail address" name="email">
                    <textarea placeholder="Your message" name="body"></textarea>
                </div>
                <div class="contact-recaptcha-container flex center">
                    <div class="recaptcha-error-container">
                        <div class="g-recaptcha" data-sitekey="6LdX7QoUAAAAAMsC6bm_KHDap-WYzuaMEsUAiOC5"></div>
                    </div>
                </div>
                <div class="send-btn">
                    <span class="send-btn-text">SEND</span> <img src="<?php echo apzumi_image_directory()?>/arror-right-red.png">
                </div>
            </form>
        </div>
    </div>

</div>
<?php
get_footer();
