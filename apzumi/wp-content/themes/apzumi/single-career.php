<?php
    get_header();
    the_post();
    $simpleFieldsConnector = null;
    if (function_exists('simple_fields_get_all_fields_and_values_for_post')) {
        $simpleFieldsConnector = simple_fields_get_all_fields_and_values_for_post(get_the_ID());
    }
    if ($simpleFieldsConnector):
?>
<div id="career-job">
    <div id="job-header" class="section flex slogan job-developer-bg">
        <div class="container">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <div id="job-body" class="section">
        <div class="container">
        <?php if (essentials_description_only($simpleFieldsConnector)): ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php echo get_the_content_table($simpleFieldsConnector, 'essentials'); ?>
                    <?php echo get_the_content_table($simpleFieldsConnector, 'description', 'description_only'); ?>
                </div>
            </div>   
        <?php else: ?>
            <div class="row">
                <div class="col-md-6">
                    <?php echo get_the_content_table($simpleFieldsConnector, 'essentials'); ?>
                    <?php echo get_the_content_table($simpleFieldsConnector, 'project_work_methodology', 'options_with_description'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo get_the_content_table($simpleFieldsConnector, 'spec'); ?>
                    <?php echo get_the_content_table($simpleFieldsConnector, 'one_on_one'); ?>
                    <?php echo get_the_content_table($simpleFieldsConnector, 'job_profile'); ?>
                    <?php echo get_the_content_table($simpleFieldsConnector, 'equipment_supplied'); ?>
                </div>
            </div>

            <?php
            $content = get_the_content_table($simpleFieldsConnector, 'other_info', 'standard_grid');
            if(!empty($content)) : ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="grid-table">
                            <?php echo $content ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-xs-12">
                    <div class="grid-table">
                        <?php echo get_the_content_table($simpleFieldsConnector, 'this_and_that', 'standard_grid'); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="grid-table">
                        <?php
                        $gridLayout = new JobOfferGridLayout();
                        echo $gridLayout->header('Requirements');
                        ?>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?php echo get_the_content_table($simpleFieldsConnector, 'requirements_must', ['tableType' => 'inside_grid', 'tableName'=>'Must']); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?php echo get_the_content_table($simpleFieldsConnector, 'requirements_nice_to_have', ['tableType' =>'inside_grid', 'tableName'=>'Nice-To-Have']); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?php echo get_the_content_table($simpleFieldsConnector, 'requirements_languages', ['tableType' =>'inside_grid', 'tableName'=>'Languages']); ?>
                                    </div>
                                </div>
                            </div>
                        <?php
                        echo $gridLayout->footer();
                        ?>
                    </div>
                </div>
            </div>
            <?php
            $content = get_the_content_table($simpleFieldsConnector, 'other_requirements', 'standard_grid');
            if(!empty($content)) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="grid-table">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>        
        <?php endif; ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="apply">
                        <button class="offer-btn" data-toggle="modal" data-target=".bs-apply-modal">
                            <span class="offer-btn-text">Apply</span>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php get_template_part('template-parts/form','apply')?>
</div>

<?php
else : //if no Simple Fields connector exists for the post we display the post content directly
    the_content();
endif;
get_footer();
