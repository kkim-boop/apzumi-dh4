<?php get_header(); ?>

<div id="software-development">

    <div id="software-development-hero" class="section flex slogan" data-offset="0" <?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
        <div class="container padded">
            <h1>You can deliver your product right away without spending a ton of money</h1>
            <h3>Our ready-to-use, battle-tested and bug-free meta component framework will turn your ideas into an effective software solution within weeks.</h3>
        </div>
        <img src="<?php echo apzumi_image_directory();?>/arrow-down.png" class="arrow-down">
    </div>

    <div class="supersection">        
        <div id="ui-ux-experts" class="section flex">
            <div class="container padded default-width">
                <div class="section-title">UI/UX experts</div>
                <img src="<?php echo apzumi_image_directory();?>/notebook.png" id="notebook">
                <div class="pull-right">
                    <h1 class="ui-ux">We guarantee <span class="red">amazing websites, systems and apps</span> that show off your business vision.</h1>
                    <h2 class="ui-ux">
                        The freshest designs and graphics create practical and helpful experiences for your customers
                    </h2>                
                </div>
            </div>
        </div>

        <div id="remote-cto" class="section flex">  
            <div class="section-title remote-cto only-mobile">Remote CTO</div>
            <img src="<?php echo apzumi_image_directory();?>/remote_cto.png" id="remote-cto-image" class="only-mobile">
            <div class="container padded default-width">
                <div class="section-title remote-cto no-mobile">Remote CTO</div>

                <h1 class="remote-cto">Allow us the role <span class="red">of technical co-founder</span> and to guide you as the CTO for your startup.</h1>
                <h2 class="remote-cto">We focus on your unique technological issues and take full responsibility for the shape and success of your IT strategy.</h2>
            </div>
        </div>

        <div id="apps" class="section flex">    
            <div id="apps-bg-holder">
                <div id="apps-bg"></div>
            </div>
                <div class="container padded relative default-width full-height flex space-between align-center desktop-row-reverse small-vertical">
                    <div id="apps-description" class="full-height">
                        <div class="full-height center">
                            <div id="apps-text-holder">
                                <span id="apps-text">APPS</span>                            
                            </div>
                            <div id="apps-text-content" class="flex vertical">
                                <div class="section-title">Mobile app development</div>
                                <h1>Our rich experience with mobile <span class="red">apps</span> and the satisfaction of our clients, demonstrate the <span class="nobr">success of</span> our useful <span class="nobr">and modern</span> apps.</h1>
                                <h2>We build effective apps for your customers that help you <span class="nobr">develop your</span> business processes.</h2>
                            </div>                    
                        </div>                
                    </div>
                    <div class="apps-image flex justify-end">                
                        <img src="<?php echo apzumi_image_directory();?>/smartphone-v2.png" id="smartphone">  
                    </div>
                </div>   
        </div>
    </div>

    <div id="custom-software-development" class="section flex">
        <div class="container padded default-width">
            <div class="section-title">Custom software development</div>
            <h1 class="custom-software-development">We effortlessly help you improve every business process. </h1>
            <h2 class="custom-software-development">From management to production, we automate processes, making them fast and easy. </h2>
            <div class="flex space-around small-justify-center align-center small-vertical">
                <img src="<?php echo apzumi_image_directory();?>/stopwatch.png" id="stopwatch">
                <img src="<?php echo apzumi_image_directory();?>/scrum-dots.svg" class="scrum-dots only-mobile">
                <div id="scrum">We use <span class="red">scrum methodology</span> when developing <span class="nobr">software and</span> by offering cloud solutions, provide you the <span class="nobr">easiest use</span> of systems.</div>
                <img src="<?php echo apzumi_image_directory();?>/scrum-dots.svg" class="scrum-dots only-mobile">
                <img src="<?php echo apzumi_image_directory();?>/software-development-cloud.png" id="software-development-cloud">
            </div>
        </div>
    </div>
    
    <div id="business-intelligence" class="section flex">
        <div class="container padded default-width">
            <div class="section-title business-intelligence">Business intelligence</div>
            <div class="flex space-between small-reverse-vertical mobile-align-center"> 
                <h1 class="business-intelligence">
                    Your decision-making processes will improve with 
                    <span class="red">our state-of-the-art</span> 
                    business analytic tools and systems based on 
                    <span class="red">artificial intelligence.</span>
                </h1>
                <img src="<?php echo apzumi_image_directory();?>/chart.png" class="chart2">
            </div>
        </div>
    </div>
    
    <div id="security" class="section flex">
        <div class="container padded default-width">
            <div><img src="<?php echo apzumi_image_directory();?>/padlock.png"></div>
            <h1>We employ tools that <span class="red">protect you</span> 
                from attack, data loss, theft or illegal distribution.
            </h1>
            <h2>Because your <span class="red">data safety</span> 
                is of the utmost importance, we guarantee 
                the highest standards of IT system protection.
            </h2>
        </div>
    </div>

    <div id="implementation" class="section flex">
        <div class="container padded default-width">
            <div class="section-title">Implementation</div>
            <div class="desktop-half-width pull-left">
                <img src="<?php echo apzumi_image_directory();?>/implementation.png" class="no-mobile">
            </div>
            <div class="desktop-half-width description">
                <h1>
                    We want to be sure you receive our <span class="red">full service</span> at every stage.
                </h1>
                <img src="<?php echo apzumi_image_directory();?>/implementation.png" class="only-mobile">
                <h2>
                    We are always aware and ready to implement new user-friendly systems and to teach your employees how to take full advantage of them.
                </h2>
            </div>


        </div>
    </div>

    <div id="maintenance" class="section flex">
        <div class="container padded default-width">
            <div class="section-title">Maintenance, quality</div>
            <img src="<?php echo apzumi_image_directory();?>/24-7-circle.png" class="no-mobile">
            <div class="description desktop-pull-left">
                <h1>
                    We provide comprehensive <span class="red">24/7/365</span> support and accept assignments for regular care of third party systems.
                </h1>
                <img src="<?php echo apzumi_image_directory();?>/24-7-circle.png" class="only-mobile">
                <h2>
                    By implementing monitoring tools, we take active steps to prevent efficiency or safety issues.
                </h2>
            </div>

        </div>
    </div>

    <div id="technology" class="section flex">
        <div class="container padded default-width">
            <div class="section-title">Technology</div>
            <div class="description">
                <div class="desktop-six-sevenths">
                    <h1>
                        Employing the <span class="red">very latest technologies</span> for your benefit is our mission.
                    </h1>                    
                    <img src="<?php echo apzumi_image_directory();?>/technology.png" class="only-mobile">
                    <h2>
                        <strong>Web:</strong> AngularJS, JAVA, .NET, PHP, JavaScript, ActionScript, GWT, Sencha / ExtJs, REST, WebServices.<br>
                        <strong>Data:</strong> PostgreSQL, MySQL, SQL Server, Oracle, MongoDB.<br>
                        <strong>Mobile:</strong> Android, iOS, PhoneGap, RWD.
                        <br><br>
                    </h2>
                </div>                
            </div>
            <img src="<?php echo apzumi_image_directory();?>/technology.png" class="no-mobile">
        </div>
    </div>

    <a class="full-width-button button button-red no-fixed" href="/contact">
        <span>LET'S TALK</span>
        <span class="right-arrow"></span>     
    </a>
    
</div>

<?php
    get_footer();

