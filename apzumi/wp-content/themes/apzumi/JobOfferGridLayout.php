<?php
/**
 * Class JobOfferGridLayout
 *
 * helper class for table layout HTML generation
 */
class JobOfferGridLayout {

    public function header($fieldGroupName)
    {
        $this->start();
        ?>
        <div class="row grid-table-header">
            <div class="col-xs-12">
                <h2><?php echo $fieldGroupName ?></h2>
            </div>
        </div>
        <div class="row grid-table-body">
        <?php
        return $this->end();
    }

    public function smallHeader($fieldGroupName)
    {
        $this->start();
        ?>
        <div class="row grid-table-header-small">
            <div class="col-xs-12">
                <h3><?php echo $fieldGroupName ?></h3>
            </div>
        </div>
        <div class="row grid-table-body">
        <?php
        return $this->end();
    }

    public function footer()
    {
        $this->start();
        ?>
        </div>
        <?php
        return $this->end();
    }

    public function itemXs12($label, $value)
    {
        if ($value=='') return '';
        $this->start();
        ?>
        <div class="col-xs-12 grid-table-item">
            <div class="row">
                <div class="col-xs-12"><?php echo $value; ?></div>
            </div>
        </div>
        <?php
        return $this->end();
    }

    public function itemOptionMd4($label, $value)
    {
        if(!$value) return '';
        $this->start();
        ?>
        <div class="col-xs-12 col-md-4 grid-table-item">
            <div class="row">
                <div class="col-xs-7 field-label"><?php echo $label; ?></div>
                <div class="col-xs-5 accept">
                <?php if ($value) : ?>
                    <img src="<?php echo apzumi_image_directory() ?>/career/confirm.svg">
                <?php else : ?>
                    <img src="<?php echo apzumi_image_directory() ?>/career/cancel-01.svg">
                <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        return $this->end();
    }

    private function generateStars($value) {
        $stars = [];
        for($i=0; $i<3; $i++) {
            if($i < intval($value)){
                $url = apzumi_image_directory() . '/career/gwiazdka_pelna.svg';
            } else {
                $url = apzumi_image_directory() . '/career/gwiazdka_pusta.svg';
            }
            $stars[] = '<img src="'.$url.'">';
        }
        return $stars;
    }

    public function itemStarsMd4($label, $value)
    {
        if(intval($value)<1) return '';
        $this->start();
        ?>
        <div class="col-xs-12 col-md-4 grid-table-item">
            <div class="row">
                <div class="col-xs-5 field-label"><?php echo $label; ?></div>
                <div class="col-xs-7 stars"><?php echo(implode(' ', $this->generateStars($value))); ?></div>
            </div>
        </div>
        <?php
        return $this->end();
    }

    public function itemStarsXs12($label, $value)
    {
        if(intval($value)<1) return '';
        $this->start();
        ?>
        <div class="col-xs-12 grid-table-item">
            <div class="row">
                <div class="col-xs-7 field-label"><?php echo $label; ?></div>
                <div class="col-xs-5 stars"><?php echo(implode(' ', $this->generateStars($value))); ?></div>
            </div>
        </div>
        <?php
        return $this->end();
    }


    /**
     * start
     * helper function for getting HTML as string
     */
    private function start()
    {
        ob_start();
    }

    /**
     * end
     * helper function for getting HTML as string
     */
    private function end()
    {
        $output = ob_get_clean();
        ob_flush();
        return $output;
    }
}