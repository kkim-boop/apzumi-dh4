<?php get_header();?>
<div id="career">
    <div id="career-apzumi" class="slogan section flex" data-offset="0" style="background-image: url('<?php echo apzumi_image_directory()?>/apzumi-bg-6.jpg ?>')">
        <div class="container padded">
            <h1>Innovators <span class="nobr">and good</span> friends</h1>
        </div>
        <img src="<?php echo apzumi_image_directory()?>/arrow-down.png" class="arrow-down">
    </div>
    <hr class="visible-xs visible-sm">
    <div id="job-offers" class="section flex" data-offset="1">
        <div class="container">
            <h1>
                Become a part of our story as:
            </h1>
            <div class="jobs-list">
                <?php
                $args = array(
                    'post_type' => 'career',
                    'post_status' => 'publish',
                    'nopaging' => true
                );
                $query = new WP_Query($args);
                while ($query->have_posts()) :
                    $query->the_post();
                    $categories = get_the_category();
                    $category = (count($categories) > 0) ? $categories[0]->name : '';

                    $employmentType = '';
                    $city = '';
                    if (function_exists('simple_fields_get_all_fields_and_values_for_post')) {
                        $simpleFieldsConnector = simple_fields_get_all_fields_and_values_for_post(get_the_ID());
                        $fieldGroup = find_field_group($simpleFieldsConnector['field_groups'], 'essentials');

                        $employmentType = trim(field_value(find_field($fieldGroup['fields'], 'employment_type')));
                        $city           = trim(field_value(find_field($fieldGroup['fields'], 'location')));
                    }
                    ?>
                    <div class="job-row">
                        <div class="row">
                            <div class="col-md-7 job-col title"><?php the_title(); ?></div>
                            <div class="col-md-3 job-col employment-type"><?php echo $employmentType; ?></div>
                            <div class="col-md-2 job-col city"><?php echo $city; ?></div>
                        </div>
                        <div class="apply">
                            <a href="<?php the_permalink();?>" class="offer-btn">
                                <span class="offer-btn-text">Apply</span> <img src="<?php echo apzumi_image_directory()?>/arror-right-red.png">
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <hr class="visible-xs visible-sm">
    <div id="challenge-yourself" class="section flex" data-offset="2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Challenge yourself</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/learn_teach.svg" alt="Learn & teach others">
                    <h2>Learn & teach<br>others</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/impact_startup_industry.svg" alt="Impact the startup industry">
                    <h2>Impact the startup<br>industry</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/fast_scaling_projects.svg" alt="Be the part of fast scaling projects">
                    <h2>Be the part<br> of fast scaling projects</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/develop_your_passions.svg" alt="Develop your passions">
                    <h2>Develop<br>your passions</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/be_yourself.svg" alt="Be yourself. Unique and meaningful">
                    <h2>Be yourself. Unique and<br>meaningfull</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory() ?>/career/inspire_get_inspired.svg" alt="Inspire & get inspired">
                    <h2>Inspire &<br>get inspired</h2>
                </div>
            </div>
        </div>
    </div>
    <div id="work-at-apzumi" class="section flex career-work-at-apzumi-bg" data-offset="3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 career-video-container">
                    <h1>What it is like to work at Apzumi</h1>
                    <?php if (function_exists('get_custom')) {
                        echo get_custom('career_video_embed_code');
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div id="we-offer" class="section flex" data-offset="4">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>We offer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-offset-1 col-md-5">
                    <ul>
                        <li><p>high salary</p></li>
                        <li><p>integration trips and meetings</p></li>
                        <li><p>mentorship period</p></li>
                        <li><p>clear promotion opportunities</p></li>
                        <li><p>project variety</p></li>
                        <li><p>health insurance</p></li>
                        <li><p>professional equipment</p></li>
                        <li><p>ambitious & skilled people who share knowledge</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-5">
                    <ul>
                        <li><p>daily diet subsidies</p></li>
                        <li><p>daily snacks</p></li>
                        <li><p>agile environment</p></li>
                        <li><p>flexible work hours</p></li>
                        <li><p>MultiSport Card</p></li>
                        <li><p>challenges and contests with prizes</p></li>
                        <li><p>learning resources<br>(books courses)</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="join-us" class="section flex no-fixed">
        <div class="container padded">
            <h1>Passion, progress, partnership</h1>
            <a id="career-join-us-up" href="#" class="contact-btn">
                Join Us <img src="<?php echo apzumi_image_directory()?>/arrow-up-blue.png">
            </a>
        </div>
    </div>
</div>

<?php
get_footer();
