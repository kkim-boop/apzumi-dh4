<?php
get_header();

$realisation = array(
    'id' => 'headmatters',
    'name' => 'Headmatters',
    'slogan' => 'Essential tools to support mental health caregivers',
    'nImages' => 3,
    'references' =>  array(
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        )
    ),
    'technologies' => array(
        "hibernate", "angularjs", "java"
    ),
    'description' => array(
        'Applications for helping people are our cup of tea. 
            Searching around the world, we found a place where such help 
            is exceptionally necessary – India, a place where people lack 
            medical care, where approximately 250 million individuals suffer 
            from some form of mental illness and per one licensed psychologist 
            there are over 300,000 inhabitants. The HeadMatters team wanted to 
            improve living conditions of people suffering from mental 
            illnesses. They decided to introduce a solution that ensures the 
            caregivers of ill people with priceless support. HeadMatters 
            provides caregivers with medical knowledge, contact with a network 
            of people in similar situations, and the expertise of specialists 
            online.',
            'We were happy to be part of such a great project. From the very 
                beginning we were fully engaged in seeing the startup to full 
                success. Development of software was only a small part of our 
                contribution. We became a technical co-founder and helped the 
                startup impress investors and secure funding. Thanks to our 
                experience with startups, the HeadMatters team felt confident 
                in the success of their product launch, allowing them to focus 
                on communicating with potential customers and partners.<br>
                <br>
                This project is not just another app, but makes life easier, 
                delivers medical resources for all people, and has the 
                potential to alter the world healthcare industry for the
                better. Everybody deserves it.'
    )
);
?>
<?php
$technologyIcons = array(
    'activiti' => 'activity.png?imgv=1.2',
    'android' => 'android.svg',
    'angularjs' => 'angularjs.png?imgv=1.2',
    'css3' => 'css3.png?imgv=1.2',
    'flex' => 'flex.png?imgv=1.2',
    'hibernate' => 'hibernate.png?imgv=1.2',
    'html5' => 'html5.png?imgv=1.2',
    'ios' => 'ios.svg',
    'java' => 'java.png?imgv=1.2',
    'less' => 'less.png?imgv=1.2',
    'mysql' => 'mysql.svg',
    'nodejs' => 'nodejs.png?imgv=1.2',
    'php' => 'php.png?imgv=1.2',
    'sass' => 'sass.png?imgv=1.2',
    'spring' => 'spring.png?imgv=1.2',
    'tomcat' => 'tomcat.svg'
);

if($realisation) {
    ?>
    <div id="realisation-page">
        <div class="slogan section flex realisation-hero align-center small-align-start justify-start" id="<?php echo $realisation['id'];?>">
            <div id="our-work-link">
                <a href="/portfolio">
                    <span></span>
                </a>
                <h3>
                    Portfolio
                </h3>
            </div>
            <div class="realisation-wrapper">
                <div class="realisation-header">
                    <h1><?php echo $realisation['name']; ?></h1>
                    <h2><?php echo $realisation['slogan']; ?></h2>
                </div>
            </div>
        </div>
        <div id="images" class="realisation-images section">
            <div id="next">
                <img src="<?php echo apzumi_image_directory()?>/next.png?imgv=1.2" class="arrow-next">
            </div>
            <div id="previous">
                <img src="<?php echo apzumi_image_directory()?>/previous.png?imgv=1.2" class="arrow-previous">
            </div>
            <div class="realisation-image-wrapper flex">
                <?php
                for ($i = $realisation['nImages'] - 1; $i >= 0 ; $i--) {
                    ?>
                    <div class="realisation-image" data-realisation="<?php echo $realisation['id'];?>" data-index="<?php echo $i;?>">
                        <img src="<?php echo apzumi_image_directory()?>/<?php echo $realisation['id'];?>_<?php echo $i;?>.jpg?imgv=1.2">
                    </div>
                    <?php
                }
                ?>
                <div class="indicator">
                    <?php
                    for ($i = 0; $i < $realisation['nImages']; $i++) {
                        ?>
                        <a class="section-link" data-index="<?php echo $i; ?>">
                            <div class="link-point"></div>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div id="stack-reference" class="section">
            <div id="technology-stack">
                <h3>
                    TECHNOLOGY STACK
                </h3>
                <div id="technologies" class="flex space-between small-vertical small-justify-center">
                    <?php
                    foreach ($realisation['technologies'] as $technology) {
                        if (array_key_exists($technology, $technologyIcons)) {
                            ?>
                            <img src="<?php echo apzumi_image_directory()?>/technologies/<?php echo $technologyIcons[$technology];?>" data-technology="<?php echo $technology; ?>">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="realisation-description" class="flex">
                <?php if(count($realisation['description']) >= 2) { ?>
                    <div class="container padded default-width">
                        <div class="col">
                            <?php echo $realisation['description'][0]; ?>
                        </div>
                        <div class="col">
                            <?php echo $realisation['description'][1]; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <a class="full-width-button button button-blue no-fixed" href="/portfolio">
            <span class="left-arrow"></span>
            <span>BACK TO PORTFOLIO</span>
        </a>
        <a class="full-width-button button button-red no-fixed" href="/contact">
            <span>LET'S TALK</span>
            <span class="right-arrow"></span>
        </a>
    </div>
<?php }
get_footer();
