
<?php
get_header();

$realisation = array(
    'id' => 'iscribes',
    'name' => 'iScribes',
    'slogan' => 'A web and mobile tool that helps physicians focus on patients',
    'nImages' => 3,
    'references' =>  array(
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png",
            'authorPosition' => "Recommendation Author"
        )
    ),
    'technologies' => array("angularjs", "java", "tomcat", "mysql", "spring", "hibernate", "ios", "android"),
    'description' => array(
        'The iScribes team had one goal when they came to us – to revolutionize the way '
        . 'doctors interact with patients. They wanted to relieve physicians from a burden of EHR documentation and '
        . 'return attention to the patients, which both patients and doctors needed badly. '
        . 'Virtually no company on the American market provided a product or service that did this right, '
        . 'and there were good reasons why, including multiple technical barriers and limits, '
        . 'such as data security, network stability, fault tolerance and lack of devices with enough '
        . 'processing power and battery life. Many organizational obstacles also existed, such as '
        . 'integration with practice management systems, locked hospital information systems, and target '
        . 'users considered to be one of the most demanding groups. All of this sounded like a great challenge, '
        . 'which is why we were immediately and fully committed.<br><br>',

        'Along with the iScribes team, we embarked on a journey through unsettled waters '
        . 'of healthcare innovation. Within six months, we went from a “mission impossible” google glass '
        . 'prototype to a highly sophisticated solution for iPhone, with a handy control that was smartwatch based. '
        . 'It was a bumpy road full of challenges and sleepless nights, but was a road worth taking.<br><br>After six months, '
        . 'iScribes onboarded their first real users, and within nine months they had their first paying customers. '
        . 'Shortly after, iScribes successfully secured their first round of funding, which is not surprising at all '
        . 'given such a great team, product and huge market. We are proud to be part of such an outstanding story!'
    )
);
?>
<?php
$technologyIcons = array(
    'activiti' => 'activity.png?imgv=1.2',
    'android' => 'android.svg',
    'angularjs' => 'angularjs.png?imgv=1.2',
    'css3' => 'css3.png?imgv=1.2',
    'flex' => 'flex.png?imgv=1.2',
    'hibernate' => 'hibernate.png?imgv=1.2',
    'html5' => 'html5.png?imgv=1.2',
    'ios' => 'ios.svg',
    'java' => 'java.png?imgv=1.2',
    'less' => 'less.png?imgv=1.2',
    'mysql' => 'mysql.svg',
    'nodejs' => 'nodejs.png?imgv=1.2',
    'php' => 'php.png?imgv=1.2',
    'sass' => 'sass.png?imgv=1.2',
    'spring' => 'spring.png?imgv=1.2',
    'tomcat' => 'tomcat.svg'
);

if($realisation) {
    ?>
    <div id="realisation-page">
        <div class="slogan section flex realisation-hero align-center small-align-start justify-start" id="<?php echo $realisation['id'];?>">
            <div id="our-work-link">
                <a href="/portfolio">
                    <span></span>
                </a>
                <h3>
                    Portfolio
                </h3>
            </div>
            <div class="realisation-wrapper">
                <div class="realisation-header">
                    <h1><?php echo $realisation['name']; ?></h1>
                    <h2><?php echo $realisation['slogan']; ?></h2>
                </div>
            </div>
        </div>
        <div id="images" class="realisation-images section">
            <div id="next">
                <img src="<?php echo apzumi_image_directory()?>/next.png?imgv=1.2" class="arrow-next">
            </div>
            <div id="previous">
                <img src="<?php echo apzumi_image_directory()?>/previous.png?imgv=1.2" class="arrow-previous">
            </div>
            <div class="realisation-image-wrapper flex">
                <?php
                for ($i = $realisation['nImages'] - 1; $i >= 0 ; $i--) {
                    ?>
                    <div class="realisation-image" data-realisation="<?php echo $realisation['id'];?>" data-index="<?php echo $i;?>">
                        <img src="<?php echo apzumi_image_directory()?>/<?php echo $realisation['id'];?>_<?php echo $i;?>.jpg?imgv=1.2">
                    </div>
                    <?php
                }
                ?>
                <div class="indicator">
                    <?php
                    for ($i = 0; $i < $realisation['nImages']; $i++) {
                        ?>
                        <a class="section-link" data-index="<?php echo $i; ?>">
                            <div class="link-point"></div>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div id="stack-reference" class="section">
            <div id="technology-stack">
                <h3>
                    TECHNOLOGY STACK
                </h3>
                <div id="technologies" class="flex space-between small-vertical small-justify-center">
                    <?php
                    foreach ($realisation['technologies'] as $technology) {
                        if (array_key_exists($technology, $technologyIcons)) {
                            ?>
                            <img src="<?php echo apzumi_image_directory()?>/technologies/<?php echo $technologyIcons[$technology];?>" data-technology="<?php echo $technology; ?>">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="realisation-description" class="flex">
                <?php if(count($realisation['description']) >= 2) { ?>
                    <div class="container padded default-width">
                        <div class="col">
                            <?php echo $realisation['description'][0]; ?>
                        </div>
                        <div class="col">
                            <?php echo $realisation['description'][1]; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <a class="full-width-button button button-blue no-fixed" href="/portfolio">
            <span class="left-arrow"></span>
            <span>BACK TO PORTFOLIO</span>
        </a>
        <a class="full-width-button button button-red no-fixed" href="/contact">
            <span>LET'S TALK</span>
            <span class="right-arrow"></span>
        </a>
    </div>
<?php }
get_footer();
