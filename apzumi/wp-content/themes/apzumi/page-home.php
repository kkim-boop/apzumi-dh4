<?php get_header(); ?>

    <div class="main">
        <div id="slogan" class="slogan no-fixed"<?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
            <div id="slogan-text1" class="section flex vertical">
                <h1>Your vision. Our tech.</h1>
                <div id="slogan-text1-slide">
                    <div id="slogan-subtext1"><h3>As your dedicated tech <strong>partner</strong> we <strong>passionately</strong> lead your business to <strong>progress</strong> and development.</h3></div>
                    <div id="slogan-subtext2"><h3>We excel with startups and companies who appreciate quality and innovation.</h3></div>
                </div>
                <script type="text/javascript" src=" https://widget.clutch.co/static/js/widget.js"></script> <div class="clutch-widget" data-url=" https://widget.clutch.co" data-widget-type="2" data-height="50" data-darkbg="1" data-clutchcompany-id="69384"></div>
                <img src="<?php echo apzumi_image_directory()?>/arrow-down.png?imgv=1.2" class="arrow-down">
            </div>
            <div id="slogan-text2" class="section flex vertical">
                <h1>What we do</h1>
                <div id="areas" class="flex space-around align-end small-vertical small-align-center">
                    <?php
                    $areas = array(
                        array(  'image' => apzumi_image_directory() . '/copy-4.png',
                            'text' => 'Digital Health <br><a href="digital-health/" class="new_read_more">Read More <img src="https://www.kimkstar.com/apzumi/wp-content/uploads/2020/04/Right-arrow.svg"></a>'),
                        array(  'image' => apzumi_image_directory() . '/new-4.png',
                            'text' => 'Software<br>Development <br><a href="software-development/" class="new_read_more">Read More <img src="https://www.kimkstar.com/apzumi/wp-content/uploads/2020/04/Right-arrow.svg"></a>'),
                        array(  'image' => apzumi_image_directory() . '/new.png',
                            'text' => 'Product strategy <br><a href="product-strategy/" class="new_read_more">Read More <img src="https://www.kimkstar.com/apzumi/wp-content/uploads/2020/04/Right-arrow.svg"></a>'),
                        array(  'image' => apzumi_image_directory() . '/new3.png',
                            'text' => 'AR/VR <br><a target="_blank" href="http://spatial.apzumi.com/" class="new_read_more">Read More <img src="https://www.kimkstar.com/apzumi/wp-content/uploads/2020/04/Right-arrow.svg"></a>')
                    );

                    foreach ($areas as $index => $area) {
                        ?>
                        <div class="area">
                            <img class="area-image ff" id="area-image-<?php echo $index; ?>" src="<?php echo $area['image'];?>">
                            <div class="area-text" id="area-text-<?php echo $index; ?>">
                                <span class="text"><?php echo $area['text'];?></span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <img src="<?php echo apzumi_image_directory()?>/arrow-down.png?imgv=1.2" class="arrow-down">
                <div class="bottom_text">
                    Pssst...You didn't find anything for yourself? Tell us about your idea. We're always open-minded.
                </div>

            </div>
        </div>
        <div id="customer-say" class="section flex no-fixed">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>The customers say</h1>
                        <?php if (function_exists('get_custom')) {
                            echo get_custom('home_video_embed_code');
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="realisations" class="section flex no-fixed">
            <img src="<?php echo apzumi_image_directory()?>/back-big.png?imgv=1.2" id="back" class="small-hidden arrow-back">
            <img src="<?php echo apzumi_image_directory()?>/main_next.png?imgv=1.2" id="next" class="small-hidden arrow-next">
            <img src="<?php echo apzumi_image_directory()?>/main_previous.png?imgv=1.2" id="previous" class="small-hidden arrow-previous">
            <div id="meedy-realisation" data-index="0" class="realisation apzumi-slide" data-realisation="meedy">
                <div class="visit-area">
                    <a class="button" href="/meedy">Visit<span class="right-arrow"></span></a>
                </div>
                <div class="content-area flex align-center justify-start small-align-start">
                    <img src="<?php echo apzumi_image_directory()?>/meedy_background.png?imgv=1.2" id="circles-image">
                    <div class="text">
                        <h1>Meedy</h1>
                        <h2>Comprehensive solution for health & insurance companies and their customers</h2>
                        <hr>
                        <div class="realisation-footer">
                            <div>
                                <span class="attribute">Industry:</span>
                                organization and settlement of medical services.
                            </div>
                            <div>
                                <span class="attribute">Platforms:</span>
                                android, iOS, web browser.
                            </div>
                            <div>
                                <span class="attribute">Availability:</span>
                                scalability and geographic redundancy.
                            </div>
                        </div>
                    </div>
                    <img id="imac-image" src="<?php echo apzumi_image_directory()?>/iMac 3_4 view left.png?imgv=1.2">
                    <img id="iphone-image" src="<?php echo apzumi_image_directory()?>/iPhone5s-PF-Silver.png?imgv=1.2">
                    <img id="meedy320-image" src="<?php echo apzumi_image_directory()?>/meedy320.png?imgv=1.2">
                </div>
            </div>
            <div id="medatex-realisation" data-index="1" class="realisation apzumi-slide" data-realisation="medatex">
                <div class="visit-area">
                    <a class="button" href="/medatex">Visit<span class="right-arrow"></span></a>
                </div>
                <div class="content-area flex align-center justify-start small-align-start">
                    <div class="text">
                        <h1>Medatex</h1>
                        <h2>A tool to manage and simplify injury claims processes</h2>
                        <hr>
                        <div class="realisation-footer">
                            <div>
                                <span class="attribute">Industry:</span>
                                insurance, medical.
                            </div>
                            <div>
                                <span class="attribute">Platforms:</span>
                                web browser.
                            </div>
                            <div>
                                <span class="attribute">Availability:</span>
                                scalability and geographic redundancy.
                            </div>
                        </div>
                    </div>
                    <img id="screens-image" src="<?php echo apzumi_image_directory()?>/SCREENS1.png?imgv=1.2">
                </div>
            </div>
            <div id="iscribes-realisation" data-index="2" class="realisation apzumi-slide" data-realisation="iscribes">
                <div class="visit-area">
                    <a class="button" href="/iscribes">Visit<span class="right-arrow"></span></a>
                </div>
                <div class="content-area flex align-center justify-start small-align-start">
                    <div class="text">
                        <h1>iScribes</h1>
                        <h2>A web and mobile tool that helps physicians focus on patients</h2>
                        <hr>
                        <div class="realisation-footer">
                            <div>
                                <span class="attribute">Industry:</span>
                                EMR
                            </div>
                            <div>
                                <span class="attribute">Platforms:</span>
                                Android, GoogleGlass, iOS, Web
                            </div>
                        </div>
                    </div>
                    <img id="iscribes-image" src="<?php echo apzumi_image_directory()?>/iscribes_frontal.png?imgv=1.2">
                </div>
            </div>
            <div class="realisation-mobile meedy-mobile container only-mobile">
                <div class="navigation">
                    <div class="pull-left back">
                        <img src="<?php echo apzumi_image_directory()?>/back.png?imgv=1.2" />
                    </div>
                    <div class="pull-right next">
                        <img src="<?php echo apzumi_image_directory()?>/right.png?imgv=1.2" />
                    </div>
                    <div class="pull-right prev">
                        <img src="<?php echo apzumi_image_directory()?>/left.png?imgv=1.2" />
                    </div>
                </div>
                <div class="col description">
                    <h1>Meedy</h1>
                    <h2>Comprehensive solution for health and insurance companies and their customers.</h2>
                    <div class="img"><img src="<?php echo apzumi_image_directory()?>/meedy-mobile.png?imgv=1.2"></div>

                    <div class="bar"></div>
                    <h3><span class="red">Industry:</span> HealthCare, Insurance <br>
                        <span class="red">Platforms:</span> Android, iOS, Web</h3>
                </div>
            </div>
            <div class="realisation-mobile iscribes-mobile container only-mobile">
                <div class="navigation">
                    <div class="pull-left back">
                        <img src="<?php echo apzumi_image_directory()?>/back.png?imgv=1.2" />
                    </div>
                    <div class="pull-right next">
                        <img src="<?php echo apzumi_image_directory()?>/right.png?imgv=1.2" />
                    </div>
                    <div class="pull-right prev">
                        <img src="<?php echo apzumi_image_directory()?>/left.png?imgv=1.2" />
                    </div>
                </div>
                <div class="col description">
                    <h1>iScribes</h1>
                    <h2>Web and mobile, HIPAA compliant solution that brings new quality to EMR industry.</h2>
                    <div class="img"><img src="<?php echo apzumi_image_directory()?>/iscribes-mobile.png?imgv=1.2"></div>

                    <div class="bar"></div>
                    <h3><span class="red">Industry:</span> HealthCare<br>
                        <span class="red">Platforms:</span> Web, iOS, Android, Google Glass </h3>
                </div>
            </div>
            <div class="realisation-mobile medatex-mobile container only-mobile">
                <div class="navigation">
                    <div class="pull-left back">
                        <img src="<?php echo apzumi_image_directory()?>/back.png?imgv=1.2" />
                    </div>
                    <div class="pull-right next">
                        <img src="<?php echo apzumi_image_directory()?>/right.png?imgv=1.2" />
                    </div>
                    <div class="pull-right prev">
                        <img src="<?php echo apzumi_image_directory()?>/left.png?imgv=1.2" />
                    </div>
                </div>
                <div class="col description">
                    <h1>Medatex</h1>
                    <h2>Manages injury claims processes.</h2>

                    <div class="img"><img src="<?php echo apzumi_image_directory()?>/medatex-mobile.png?imgv=1.2"></div>

                    <div class="bar"></div>
                    <h3><span class="red">Industry:</span> HealthCare, Insurance <br>
                        <span class="red">Platforms:</span> Web, iOS, Android </h3>
                </div>
            </div>
            <div class="indicator">
                <?php
                for ($i = 0; $i < 3; $i++) {
                    ?>
                    <a class="section-link" data-index="<?php echo $i; ?>">
                        <div class="link-point"></div>
                    </a>
                    <?php
                }
                ?>
            </div>
        </div>
        <a id="all-realisations" class="full-width-button button button-crimson no-fixed" href="/portfolio">
            <span>SEE ALL OUR REALISATIONS</span>
            <span class="right-arrow"></span>
        </a>
        <?php
        $references = array(
            array(
                'text' => "The entire Apzumi team exceeded our expectations when they supported us with the development of our platform. Thanks to an outstanding team of highly motivated front- and back-end specialist we were able to take our platform to the next level. Lukasz is a very strong, implementation-oriented CEO, driven by the desire to never accept a “no” but rather always push for higher aspirations. In doing so, he built an exceptional team as well as an excellent track record of overachieving his customers' expectations. We can strongly recommend Apzumi as a reliable partner of IT outsourcing services.",
                'authorName' => "David Nothacker",
                'authorPosition' => "Founder & CEO at sennder GmbH"
            ),
            array(
                'text' => "Lukasz and the Apzumi team have been excellent partners to work with. Over the course of several shifting demands and constraints they have helped to develop and refine our mental health startup/app and have done so with enthusiasm and a high degree of invaluable expertise. In my opinion Lukasz and the team he has constructed at Apzumi present an ideal opportunity for any startup looking to take their idea to implementation and beyond.",
                'authorName' => "Cavin Ward-Caviness",
                'authorPosition' => "Postdoctoral Researcher at Helmholtz Zentrum München"
            ),
            array(
                'text' => "Lukasz and his team (specifically, Sebastian) have been extremely responsive to my requests and his products are of the highest quality. My research team at Duke University intends on using Lukasz's company for all of our future IT-related projects. We thank them for their ongoing support! Sophia.",
                'authorName' => "Sophia Smith",
                'authorPosition' => "Associate Professor at Duke University"
            )
        );
        ?>
        <div id="main-references" class="section flex vertical">
            <h1>Clients & Accomplishments</h1>
            <div id="accomplishments-container">
                <?php foreach ($references as $index => $reference): ?>
                    <div id="clients-accomplishments-<?php echo $index; ?>" data-index="<?php echo $index; ?>" class="main-references apzumi-slide">
                        <div class="text">
                            <div class="quote">
                                “
                            </div>
                            <div class="main">
                                <div class="clients-accomplishments-main">
                                    <?php echo $reference['text']; ?>
                                </div>
                                <div class="clients-accomplishments-footer">
                                    <div class="author">
                                        <div class="name"><?php echo $reference['authorName']; ?></div>
                                        <div class="company"><?php echo $reference['authorPosition']; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="indicator-container">
                    <div class="content-area flex align-end justify-center">
                        <div class="text">
                            <div class="indicator">
                                <?php
                                for ($i = 0; $i < count($references); $i++) {
                                    ?>
                                    <a class="section-link" data-index="<?php echo $i; ?>">
                                        <div class="link-point"></div>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact" class="section flex no-fixed">
            <div class="container padded">
                <div class="section-title">contact us</div>
                <h1>Drop us a line</h1>
                <h2>
                    <div>We’ll estimate your project</div>
                    <div>We’ll help you to get funding</div>
                    <div>We’ll <span class="red">make your app come true</span></div>
                </h2>
                <a href="/contact" class="contact-btn">
                    LET'S WORK TOGETHER <img src="<?php echo apzumi_image_directory()?>/arror-right-blue.png?imgv=1.2">
                </a>
            </div>
        </div>
    </div>
<?php
get_footer();