<?php

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

function apzumi_setup() {    
    add_filter('upload_mimes', 'cc_mime_types');
    
    add_theme_support('post-thumbnails'); 
    add_theme_support('post-formats');
    add_theme_support('html5');
    add_theme_support('editor-style');
    add_theme_support('widgets');
    add_theme_support('menus');
    add_theme_support('post-formats', array(
		'image'
    ) );
        
    register_nav_menus( array(
	'main' => __( 'Main Menu', 'apzumi' )
    ) );
       
    add_editor_style( array( get_template_directory_uri() . '/css/bootstrap.min.css') );
    add_editor_style( array( get_template_directory_uri() . '/css/main.css') );   
    add_editor_style( array( get_template_directory_uri() . '/css/apzumi-divi.css'));
    add_editor_style( array( get_template_directory_uri() . '/css/main-mobile.css') );
    add_editor_style( array( get_template_directory_uri() . '/css/vendor/jquery-ui.min.css') );
    add_editor_style( array( get_template_directory_uri() . '/css/vendor/jquery.bxslider.css') );
}

add_action( 'after_setup_theme', 'apzumi_setup' );

function apzumi_image_directory() {
    return get_template_directory_uri() . '/img';
}

function apzumi_scripts() {
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '1.0');
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', array(), '1.5.4');
    wp_enqueue_style('main-mobile', get_template_directory_uri() . '/css/main-mobile.css', array(), '1.1.2');
    wp_enqueue_style('apzumi-divi', get_template_directory_uri() . '/css/apzumi-divi.css', array(), '1.0.0');
    wp_enqueue_script('jquery-3.1.1', get_template_directory_uri() . '/js/vendor/jquery-3.1.1.min.js', array(), '3.1.1', true);
    if (function_exists('get_custom')) {
        wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . get_custom('google_maps_key'), array(), '1.0', false);
        wp_enqueue_script('contact', get_template_directory_uri() . '/js/contact.js', array('jquery', 'google-maps'), '1.2.0', true);
    }
    wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/js/vendor/jquery-ui.min.js', array('jquery-3.1.1'), '1.0', true);
    wp_enqueue_script('hammer', get_template_directory_uri() . '/js/vendor/hammer.min.js', array('jquery-3.1.1'), '1.0', true);
    wp_enqueue_script('greensock', get_template_directory_uri() . '/js/vendor/greensock-js/jquery.gsap.min.js', array(), '1.0', true);
    wp_enqueue_script('greensock-TimeLineLite', get_template_directory_uri() . '/js/vendor/greensock-js/TimelineLite.min.js', array(), '1.0', true);
    wp_enqueue_script('greensock-TweenLite', get_template_directory_uri() . '/js/vendor/greensock-js/TweenLite.min.js', array(), '1.0', true);
    wp_enqueue_script('greensock-css', get_template_directory_uri() . '/js/vendor/greensock-js/plugins/CSSPlugin.min.js', array(), '1.0', true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0', true);
    wp_enqueue_script('google-recaptcha', 'https://www.google.com/recaptcha/api.js', array(), '1.0', true);
    wp_enqueue_script('plugins', get_template_directory_uri() . '/js/plugins.js', array(), '1.0', true);
    wp_enqueue_script('apzumi-common', get_template_directory_uri() . '/js/common.js', array('jquery-3.1.1'), '1.0', true);
    wp_enqueue_script('scroll', get_template_directory_uri() . '/js/scroll.js', array('jquery-3.1.1'), '1.0.2', true);
    wp_enqueue_script('apzumi-slider', get_template_directory_uri() . '/js/apzumi-slider.js', array('jquery-3.1.1'), '1.0.3', true);
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array('jquery-3.1.1'), '1.0.9', true);
    wp_enqueue_script('realisation', get_template_directory_uri() . '/js/realisation.js', array('jquery-3.1.1'), '1.0', true);
    wp_enqueue_script('work', get_template_directory_uri() . '/js/work.js', array('jquery-3.1.1'), '1.0', true);
}

add_action( 'wp_enqueue_scripts', 'apzumi_scripts' );

/*function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
*/

function remove_version() {
    return '';
}

add_filter('the_generator', 'remove_version');

add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'portfolio',
        array(
            'labels' => array(
            'name' => __( 'Portfolio' ),
            'singular_name' => __( 'Portfolio' )
        ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail')
        )
    );
    
    register_post_type( 'career',
        array(
            'labels' => array(
            'name' => __( 'Career' ),
            'singular_name' => __( 'Career' )
        ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'thumbnail')
        )
    );
}

function footer_widgets_init() {
    	register_sidebar( array(
		'name'          => 'Footer area 1',
		'id'            => 'footer_area_1',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) );
        
    	register_sidebar( array(
		'name'          => 'Footer area 2',
		'id'            => 'footer_area_2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) );   
        
    	register_sidebar( array(
		'name'          => 'Footer area 3',
		'id'            => 'footer_area_3',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) );     
        
    	register_sidebar( array(
		'name'          => 'Footer area 4',
		'id'            => 'footer_area_4',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) ); 
        
    	register_sidebar( array(
		'name'          => 'Footer area 5',
		'id'            => 'footer_area_5',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) ); 
        
    	register_sidebar( array(
		'name'          => 'Footer area 6',
		'id'            => 'footer_area_6',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) );
        
    	register_sidebar( array(
		'name'          => 'Footnotes',
		'id'            => 'footnotes',
		'before_widget' => '<div class="pull-left">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="hidden">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'footer_widgets_init' );

function apzumi_body_classes($classes) {
    $postType = get_post_type();
    if ($postType === 'post' || $postType === 'career' && !is_archive()) {
        $classes[] = "std-scroll";
    }

    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iphone')!==false || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipod')!==false || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad')!==false) {
        $classes[] = "ios-version";
    }

    return $classes;
}

add_filter( 'body_class', 'apzumi_body_classes' );

//Career-post-specific functions

function essentials_description_only($connector) {
    $settingsGroup = find_field_group($connector['field_groups'], 'settings');
    if ($settingsGroup) {
        $essentialsDescriptionOnlyField = find_field($settingsGroup['fields'], 'essentials_description_only');
        if ($essentialsDescriptionOnlyField) {
            return $essentialsDescriptionOnlyField['saved_values'][0] === '1';
        }
    }
    return false;
}

function field_value($field) {
    $savedValue = $field['saved_values'][0];
    switch ($field['type']) {
        case 'checkbox' :
            return $savedValue === '1';
        case 'dropdown' :
            return $field['type_dropdown_options'][$savedValue]['value'];
        default :
            return $savedValue;
    }    
}

function find_field_group($fieldGroups, $slug) {
    foreach ($fieldGroups as $group) {
        if ($group['slug'] === $slug) {
            return $group;
        }
    }
    return null;    
}

function find_field($fields, $slug) {
    foreach ($fields as $field) {
        if ($field['slug'] === $slug) {
            return $field;
        }
    }
    return null;
}

include_once('JobOfferFieldGroup.php');
include_once('JobOfferGridLayout.php');
include_once('JobOfferTableLayout.php');

function get_the_content_table($connector, $fieldGroupName, $tableType = 'default')
{
    $fieldGroup = find_field_group($connector['field_groups'], $fieldGroupName);
    if (!$fieldGroup) {
        return;
    }

    $jobFieldGroup = new JobOfferFieldGroup($fieldGroup, $tableType);
    $html = $jobFieldGroup->generateHtml();

    return $html;
}

// add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
// add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );

// function sdt_remove_ver_css_js( $src, $handle ) 
// {
// 	$handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

// 	if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
// 	$src = remove_query_arg( 'ver', $src );

// 	return $src;
// }

