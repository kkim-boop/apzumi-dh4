<?php
get_header();

$realisation = array(
    'id' => 'medatex',
    'name' => 'Medatex',
    'slogan' => 'A tool to manage and simplify injury claims processes',
    'nImages' => 3,
    'references' =>  array(
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        )
    ),
    'technologies' => array(
        "html5", "css3", "angularjs", "java", "mysql", "spring", "hibernate"
    ),
    'description' => array(
        'Medatex is the leader of injury claim processing services on the '
        . 'Polish domestic market. They wanted to realize their mission of converting the '
        . 'latest knowledge and medical technology into value for insurance providers. '
        . 'The solution we delivered automates the processing of personal injury claims '
        . 'according to law and business regulations. It delivers analytical tools for claims '
        . 'adjusters, automates the process of judging the condition of the victim, '
        . 'the scale of damage suffered, and the amount of compensation.<br><br>'
        . 'Our task was not only to make a quality product, but to implement it in accordance '
        . 'with the old system, old database and a client who was accustomed to '
        . 'the old framework.<br><br>',

        'Thanks to the new usability and convenient user '
        . 'experience side, users engaged with the system very fast. '
        . 'Even nontechnical users could change the algorithm according to '
        . 'changes in the rules of information processing. This usability, '
        . '“the process engine”, enables users to easily change the algorithm '
        . 'according to changes in law and other requirements with no interference '
        . 'in other parts of the system. Such usability makes the whole process flexible.'
        . '<br><br>The company serves clients more effectively and delivers fast '
        . 'and accurate medical expertise. Feelings of modernity and comfort were '
        . 'brought to all groups of clients. Today, almost all domestic '
        . 'insurance providers take advantage of Medatex.'
    )
);
?>
<?php
$technologyIcons = array(
    'activiti' => 'activity.png?imgv=1.2',
    'android' => 'android.svg',
    'angularjs' => 'angularjs.png?imgv=1.2',
    'css3' => 'css3.png?imgv=1.2',
    'flex' => 'flex.png?imgv=1.2',
    'hibernate' => 'hibernate.png?imgv=1.2',
    'html5' => 'html5.png?imgv=1.2',
    'ios' => 'ios.svg',
    'java' => 'java.png?imgv=1.2',
    'less' => 'less.png?imgv=1.2',
    'mysql' => 'mysql.svg',
    'nodejs' => 'nodejs.png?imgv=1.2',
    'php' => 'php.png?imgv=1.2',
    'sass' => 'sass.png?imgv=1.2',
    'spring' => 'spring.png?imgv=1.2',
    'tomcat' => 'tomcat.svg'
);

if($realisation) {
    ?>
    <div id="realisation-page">
        <div class="slogan section flex realisation-hero align-center small-align-start justify-start" id="<?php echo $realisation['id'];?>">
            <div id="our-work-link">
                <a href="/portfolio">
                    <span></span>
                </a>
                <h3>
                    Portfolio
                </h3>
            </div>
            <div class="realisation-wrapper">
                <div class="realisation-header">
                    <h1><?php echo $realisation['name']; ?></h1>
                    <h2><?php echo $realisation['slogan']; ?></h2>
                </div>
            </div>
        </div>
        <div id="images" class="realisation-images section">
            <div id="next">
                <img src="<?php echo apzumi_image_directory()?>/next.png?imgv=1.2" class="arrow-next">
            </div>
            <div id="previous">
                <img src="<?php echo apzumi_image_directory()?>/previous.png?imgv=1.2" class="arrow-previous">
            </div>
            <div class="realisation-image-wrapper flex">
                <?php
                for ($i = $realisation['nImages'] - 1; $i >= 0 ; $i--) {
                    ?>
                    <div class="realisation-image" data-realisation="<?php echo $realisation['id'];?>" data-index="<?php echo $i;?>">
                        <img src="<?php echo apzumi_image_directory()?>/<?php echo $realisation['id'];?>_<?php echo $i;?>.png?imgv=1.2">
                    </div>
                    <?php
                }
                ?>
                <div class="indicator">
                    <?php
                    for ($i = 0; $i < $realisation['nImages']; $i++) {
                        ?>
                        <a class="section-link" data-index="<?php echo $i; ?>">
                            <div class="link-point"></div>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div id="stack-reference" class="section">
            <div id="technology-stack">
                <h3>
                    TECHNOLOGY STACK
                </h3>
                <div id="technologies" class="flex space-between small-vertical small-justify-center">
                    <?php
                    foreach ($realisation['technologies'] as $technology) {
                        if (array_key_exists($technology, $technologyIcons)) {
                            ?>
                            <img src="<?php echo apzumi_image_directory()?>/technologies/<?php echo $technologyIcons[$technology];?>" data-technology="<?php echo $technology; ?>">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="realisation-description" class="flex">
                <?php if(count($realisation['description']) >= 2) { ?>
                    <div class="container padded default-width">
                        <div class="col">
                            <?php echo $realisation['description'][0]; ?>
                        </div>
                        <div class="col">
                            <?php echo $realisation['description'][1]; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <a class="full-width-button button button-blue no-fixed" href="/portfolio">
            <span class="left-arrow"></span>
            <span>BACK TO PORTFOLIO</span>
        </a>
        <a class="full-width-button button button-red no-fixed" href="/contact">
            <span>LET'S TALK</span>
            <span class="right-arrow"></span>
        </a>
    </div>
<?php }
get_footer();
