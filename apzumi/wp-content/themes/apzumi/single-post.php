<?php 
    get_header();
    the_post();
?>

<div id="blog-post">

    <div id="blog-post-header" class="section flex slogan" 
        <?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
        <div class="container">
            <div class="post-category"><?php echo get_the_category()[0]->name; ?></div>
            <h1><?php the_title() ?></h1>
            <h3><?php echo post_custom('wps_subtitle'); ?></h3>
            <div class="date"><?php the_date() ?></div>
        </div>
        <div class="author-container">
            <?php echo get_avatar( get_the_author_meta( 'ID' ), 51 ); ?>
            <div>by <?php the_author() ?></div>
        </div>
    </div>
    <div id="blog-post-body" class="section">

        <?php the_content(); ?>

        <div class="author">
            <div class="ico-col">
                <?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?>
            </div>
            <div class="main-col">
                <h2><?php the_author() ?></h2>
                <p>
                    <?php the_author_meta('description'); ?>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div id="blog-post-like" class="container-fluid section">
    <div class="row">
        <div class="col-xs-12 header">
            <h2>You might also like</h2>
        </div>
    </div>

    <?php

    function postItemLayout($post) {
        if (!empty($post)) {
            ob_start();
            ?>
            <div class="col-xs-12 col-md-4">
                <div class="blog-post-box">
                    <div class="blog-post-category"><?php echo get_the_category($post["ID"])[0]->name; ?></div>
                    <h2><a href="<?php echo get_permalink($post['ID'])?>"><?php echo($post['post_title']) ?></a></h2>
                    <div class="blog-post-date">
                        <span class="date"><?php echo date( get_option('date_format'), strtotime( $post['post_date'] ) ); ?></span>
                        |
                        <span class="author">by <?php the_author_meta('display_name', $post['post_author'])?></span>
                    </div>
                </div>
            </div>
            <?php
            $postLayout = ob_get_clean();
            ob_flush();
            return $postLayout;
        } else {
            return '';
        }
    }

    $recent_posts = wp_get_recent_posts([ 'numberposts' => 6 ]);

    $postsLayouts = [];
    $postsInRow = 3;
    $rowCount = 0;
    foreach($recent_posts as $idx => $recent){
        if ($idx % $postsInRow == 0) {
            $postsLayouts[$rowCount] = [];
            $rowCount++;
        }
        $postsLayouts[$rowCount-1][] = postItemLayout($recent);
    }
    wp_reset_query();
    foreach ($postsLayouts as $row) {
        ?>
        <div class="row">
            <?php echo( implode("\n",$row) ); ?>
        </div>
        <?php
    }
    ?>
</div>

<?php get_footer(); ?>