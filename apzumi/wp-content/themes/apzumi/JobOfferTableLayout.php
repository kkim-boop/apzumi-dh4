<?php
/**
 * Class JobOfferTableLayout
 *
 * helper class for table layout HTML generation
 */
class JobOfferTableLayout {

    public function header($fieldGroupName)
    {
        $this->start();
        ?>
        <table class="content-table">
        <thead>
        <tr>
            <th colspan="2"><?php echo $fieldGroupName ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        return $this->end();
    }

    public function footer()
    {
        $this->start();
        ?>
        </tbody>
        </table>
        <?php
        return $this->end();
    }

    public function tr($content)
    {
        $this->start();
        ?>
        <tr>
            <?php echo $content; ?>
        </tr>
        <?php
        return $this->end();
    }

    public function th($label)
    {
        $this->start();
        ?>
        <th>
            <?php echo $label; ?>
        </th>
        <?php
        return $this->end();
    }

    public function td($value)
    {
        $this->start();
        ?>
        <td>
            <?php echo $value; ?>
        </td>
        <?php
        return $this->end();
    }

    public function tdOperatingSystems($value)
    {
        $value = explode(',', $value);
        $systems = [];
        foreach ($value as $key => $system) {
            $system = strtolower(trim($system));
            if ($system === 'windows') {
                $systems['Windows'] = 'fa-windows';
            } elseif ($system === 'macos') {
                $systems['MacOS'] = 'fa-apple';
            } elseif ($system === 'linux') {
                $systems['Linux'] = 'fa-linux';
            }
        }
        $this->start();
        ?>
        <td class="operating-systems">
        <?php foreach($systems as $label => $system) :?>
            <span class="fa <?php echo($system); ?>"></span>
        <?php endforeach; ?>
        </td>
        <?php
        return $this->end();
    }

    public function tdDisplayBar($value)
    {
        $this->start();
        ?>
        <td>
            <div class="gauge-outer-container">
                <div class="gauge-container">
                    <div class="gauge">
                        <div class="colored" style="width: <?php echo $value; ?>"></div>
                    </div>
                </div>
                <div class="value-container"><?php echo $value; ?></div>
            </div>
        </td>
        <?php
        return $this->end();
    }

    public function tdDisplayBarSum($value, $start)
    {
        $this->start();
        ?>
        <td>
            <div class="gauge-sum-container">
                <hr>
                <div class="gauge-sum">
                    <div class="empty" style="width: <?php echo $start; ?>%"></div>
                    <div class="colored" style="width: <?php echo $value; ?>%"></div>
                </div>
            </div>
        </td>
        <?php
        return $this->end();
    }

    public function tdWidthOption($value, $description)
    {
        $this->start();?>
        <td>
            <?php if ($value) : ?>
                <div class="option-img-cont">
                    <img src="<?php echo apzumi_image_directory() ?>/career/confirm.svg">
                </div>
            <?php else : ?>
                <div class="option-img-cont">
                    <img src="<?php echo apzumi_image_directory() ?>/career/cancel-01.svg">
                </div>
            <?php endif; ?>
            <div class="option-desc-cont">
                <?php echo $description ?>
            </div>
        </td>
        <?php
        return $this->end();
    }


    /**
     * start
     * helper function for getting HTML as string
     */
    private function start()
    {
        ob_start();
    }

    /**
     * end
     * helper function for getting HTML as string
     */
    private function end()
    {
        $output = ob_get_clean();
        ob_flush();
        return $output;
    }
}