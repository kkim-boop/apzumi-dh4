<!doctype html>
<html>
    <head>
        <!-- Google Analytics -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-64336271-1', 'auto');
        ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PB8RPM');</script>
        <!-- End Google Tag Manager -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Apzumi</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo apzumi_image_directory();?>/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo apzumi_image_directory();?>/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo apzumi_image_directory();?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo apzumi_image_directory();?>/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo apzumi_image_directory();?>/favicon/favicon-16x16.png">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo apzumi_image_directory();?>/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">        

        <?php if (function_exists('get_custom')) : ?>
            <script src="https://use.typekit.net/<?php echo trim(get_custom('typekit'))?>.js"></script>
            <script>try{Typekit.load({ async: false });}catch(e){}</script>
        <?php endif; ?>
        
        <?php 
            wp_head();
        ?>
    </head>

<body <?php body_class(); ?>>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PB8RPM"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php
        $iosVersion = false;
        if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iphone')!==false || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipod')!==false || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad')!==false) {
            $iosVersion = true;
        }
        ?>
        <div id="top1" class="flex align-center space-between"<?php if (!is_home() && !$iosVersion && has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img class="logo-img" src="<?php echo apzumi_image_directory();?>/logo.png"><img class="side-menu-logo-img" src="<?php echo apzumi_image_directory();?>/logo_side_menu.png"></a>
            <div class="menu">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'main'
                    ) );
                ?>                
            </div>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>research-projects/" class="header-ue-flag"><img class="header-ue-flag-img" src="<?php echo apzumi_image_directory();?>/UE_EFRR_rgb-1.jpg"></a>
            <a class="side-menu-button only-mobile">
                <img src="<?php echo apzumi_image_directory();?>/MENU_ICON.svg">
            </a>
            <img src="<?php echo apzumi_image_directory();?>/x.png" class="side-menu-close">
        </div>

        <div id="side-menu">
            <?php 
                wp_nav_menu( array(
                    'theme_location' => 'main'
                ) );
            ?>
        </div>