var initialReference = 0;
var currentReference = initialReference;
var nReferences = 0;
var nRealisations = 0;
var realisationSlider = null;
var referencesSlider = null;

var referenceDoublequoteAnimationTime = 400;
var referenceTextAnimationTime = 600;
var referenceAnimationIntervalTime = 20000;

var referenceAnimationInterval;

function hideReferences() {
    $('.reference').hide();
    $('.reference-link').removeClass('selected'); 
    $('#references .section-link').removeClass('selected');    
}

function showCurrentReference() {
    hideReferences();
    $('#references .section-link[data-index="' + currentReference + '"]').addClass('selected');
    $('.reference-link[data-index="' + currentReference + '"]').addClass('selected');
    $reference = $('.reference[data-index="' + currentReference + '"]').show(); 
    $author = $reference.find('.author').attr('style', 'left: -100%; display: none;');
    $reference.find('.doublequote-container').hide().animate({opacity: 'toggle'}, referenceDoublequoteAnimationTime);
    $text = $reference.find('.text').attr('style', 'left: 100%; display: none;');
    setTimeout( function() {
           $text.animate({left: '-=100%', opacity: 'toggle'}, referenceTextAnimationTime, 'easeInCubic');
    }, referenceDoublequoteAnimationTime);
    setTimeout( function() {
           $author.animate({left: '+=100%', opacity: 'toggle'}, referenceTextAnimationTime, 'easeInCubic');
    }, referenceTextAnimationTime);    
}

function setReferenceAnimationInterval() {    
    referenceAnimationInterval = setInterval(function() {
        currentReference = (currentReference + 1) % nReferences;    
        showCurrentReference();
    }, referenceAnimationIntervalTime);
}

// TODO: decide when to reset section (for example, if less than 50% of section was already displayed)

function resetSection(section) {
    if (section === 'references') {
        hideReferences();
        clearInterval(referenceAnimationInterval);
        currentReference = initialReference;
        setTimeout( function() {        
            showCurrentReference();
            setReferenceAnimationInterval();
        }, scrollAnimationTime);
    } else if (section === 'realisations') {
        realisationSlider.displaySlide('down');
    } else if (section === 'main-references') {
        referencesSlider.displaySlide('down');
    }
}

var currentSubslogan = 1;

$(document).ready(function() {

    realisationSlider = new apzumiSlider({
        sliderContainerId: '#realisations',
        slideSelector: '.apzumi-slide',
        numberedSlideSelectorFunction: function(index) { return '.apzumi-slide[data-index="' + index + '"]'},
        initialSlide: 0,
        nSlides: $('#realisations > .realisation').length,
        currentSlide: 0,
        slideAnimationTimeoutTime: 14000,
        slideTime: scrollAnimationTime / 2000 * 4,
        autoSlide: true,
        innerSlideAnimationCallback: function($slide, direction, timeline, delay) {
            var ease = Power2.easeOut;
            if ($(window).width() >= 980) {
                switch ($slide.attr('id')) {
                    case 'meedy-realisation' :
                        var circlesImage = $slide.find('#circles-image');
                        var imacImage = $slide.find('#imac-image');
                        var iphoneImage = $slide.find('#iphone-image');
                        var text = $slide.find('.text');

                        ease = Power4.easeOut;
                        var images = circlesImage;
                        var imagesAttributes = {left: '-=8%', ease: ease};
                        if (direction !== 'right') {
                            images = [circlesImage, imacImage];
                        }
                        timeline.from(images, 1.5, imagesAttributes, delay)
                            .from(text, 1.5, {left: '62%', opacity: 0, ease: ease}, delay)
                            .from(iphoneImage, 1.5, {left: '-12%', ease: ease}, delay + 0.1);
                        break;
                    case 'medatex-realisation' :
                        var screensImage = $slide.find('#screens-image');
                        var text = $slide.find('.text');
                        timeline.from(screensImage, 1.5, {left: '+=3%', ease: ease}, delay)
                            .from(text, 1.5, {left: '-=3%', ease: ease}, delay);
                        break;
                    case 'iscribes-realisation' :
                        var iscribesImage = $slide.find('#iscribes-image');
                        var text = $slide.find('.text');

                        timeline.from(iscribesImage, 1, {left: '+=3%', ease: ease}, delay)
                            .from(text, 1.5, {left: '+=4.5%', ease: ease}, delay);
                        break;
                    default :
                        break;
                }
            }
        }
    });

    referencesSlider = new apzumiSlider({
        sliderContainerId: '#main-references',
        slideSelector: '.apzumi-slide',
        numberedSlideSelectorFunction: function(index) { return '.apzumi-slide[data-index="' + index + '"]'},
        initialSlide: 0,
        nSlides: $('#accomplishments-container > .main-references').length,
        currentSlide: 0,
        slideAnimationTimeoutTime: 14000,
        slideTime: scrollAnimationTime / 2000 * 4,
        autoSlide: true,
        innerSlideAnimationCallback: function($slide, direction, timeline, delay) {

            var ease = Power4.easeOut;
                        var h1 = $slide.find('h1');
                        var quote = $slide.find('.quote');
                        var main = $slide.find('.clients-accomplishments-main');
                        var footer = $slide.find('.clients-accomplishments-footer');

                        timeline.from(h1, 1.5, {opacity: 0, ease: ease}, delay)
                            .from(quote, 1.5, {left: '-=7%', opacity: 0, ease: ease}, delay)
                            .from(main, 1.5, {left: '+=7%', opacity: 0, ease: ease}, delay + 0.1)
                            .from(footer, 1.5, {left: '-=5%', opacity: 0, ease: ease}, delay + 0.1);
                        
              
        }
    });

    testimonialsSlider = new apzumiSlider({
      sliderContainerId: '#testimonials',
      slideSelector: '.apzumi_testimonial_slide',
      numberedSlideSelectorFunction: function(index) { return '.apzumi_testimonial_slide_' + index },
      initialSlide: 0,
      nSlides: $('#testimonials > .apzumi_testimonial_slide').length,
      currentSlide: 0,
      slideAnimationTimeoutTime: 14000,
      slideTime: scrollAnimationTime / 2000 * 4,
      autoSlide: true
    });

    projectsSlider = new apzumiSlider({
      sliderContainerId: '#projects',
      slideSelector: '.apzumi_project_slide',
      numberedSlideSelectorFunction: function(index) { return '.apzumi_project_slide_' + index },
      initialSlide: 0,
      nSlides: $('#projects > .apzumi_project_slide').length,
      currentSlide: 0,
      slideAnimationTimeoutTime: 14000,
      slideTime: scrollAnimationTime / 2000 * 4,
      autoSlide: false
    });
    
    
    $('#slogan-text1 h1').animate({opacity: 'toggle'}, 1200);

    showCurrentReference();

    $('#slogan-text1-slide div').attr('style', 'right: -75%; display: none');
    $('#slogan-subtext1 h3').show();
    $('#slogan-subtext2 h3').show();
    
    setTimeout(function() {        
        $('#slogan-subtext' + currentSubslogan).animate({right: '+=75%', opacity: 'toggle'}, 600, 'easeInCubic');
    }, 1200);

    setReferenceAnimationInterval();    
    
    setInterval(function() {       
        var nextSubslogan = currentSubslogan%2 + 1;      
        $('#slogan-subtext' + nextSubslogan).attr('style', 'right: -75%; display: none'); 
        $('#slogan-subtext' + currentSubslogan).animate({right: '+=75%', opacity: 'toggle'}, 600, 'easeInCubic');
        $('#slogan-subtext' + nextSubslogan).animate({right: '+=75%', opacity: 'toggle'}, 600, 'easeInCubic');        
        currentSubslogan = nextSubslogan;
    }, 5000);

    var videoButtonBind = function (videoElId) {
        var $videoButton, $videoParent;
        var $videoEl = $(videoElId);

        if($videoEl) {
            $videoParent = $videoEl.parent();
            $videoButton = $videoParent.find('.video-overlay-video-btn');
        }
        if ($videoButton) {
            $videoEl.on('play', function(){
                $videoButton.hide();
            });

            $videoEl.on('pause', function(){
                $videoButton.show();
            });

            $videoButton.on('click', function(){
                $videoEl[0].play();
            });
        }
    }

    videoButtonBind('#main-video');
    videoButtonBind('#what-is-it-like-video');

    
    $('.reference-link').click(function () {
        clearInterval(referenceAnimationInterval);
        currentReference = $(this).attr('data-index');    
        showCurrentReference();  
        setReferenceAnimationInterval();
    });

    $('#references .section-link').click(function () {
        clearInterval(referenceAnimationInterval);
        currentReference = $(this).attr('data-index');    
        showCurrentReference();  
        setReferenceAnimationInterval();
    });

    
    $('.menu-item-has-children > a').click(function(e) {
        e.preventDefault();
        return false;
    }); 
    ///////////////////////// realisation slider //////////////////////////////
    var realisations = document.getElementById('realisations');
    if (!!realisations) {

        realisationSlider.setup();

        $('.visit-area').hover(
            function() {
                realisationSlider.clearAnimationTimeout();
                realisationSlider.enqueuedAnimationDirection = null;
    
                var content = $(this).parent().find('.content-area');
                var button = $(this).find('a');
    
                var timeline = new TimelineLite();
                timeline.to(content, 0.4, {opacity: '0.35'})
                    .to(button, 0.4, {opacity: '1'}, 0);
    
            },
            function() {
                var content = $(this).parent().find('.content-area');
                var button = $(this).find('a');
    
                var timeline = new TimelineLite();
                timeline.to(content, 0.4, {opacity: '1'})
                    .to(button, 0.4, {opacity: '0'}, 0);
    
                realisationSlider.setAnimationTimeout();
            });
    
    
        
        if (!!('ontouchstart' in window)) {
            var hammer = new Hammer(realisations);
            hammer.on('swipeleft', function() {
                realisationSlider.displayNextSlide('right');
            });
            hammer.on('swiperight', function() {
                realisationSlider.displayNextSlide('left');
            });
            $('.realisation .content-area').click(function() {
                window.location = "/" + $(this).parent().data('realisation');
            });
        }
    }
    
    
        ///////////////////////// references slider //////////////////////////////
    var mainReferences = document.getElementById('main-references');
    if (!!mainReferences) {
        referencesSlider.setup();
    
        if (!!('ontouchstart' in window)) {
            
            var hammer = new Hammer(mainReferences);
            hammer.on('swipeleft', function() {
                referencesSlider.displayNextSlide('right');
            });
            hammer.on('swiperight', function() {
                referencesSlider.displayNextSlide('left');
            });
        }
    }
    
        ///////////////////////// testimonials slider //////////////////////////////
    
    var testimonials = document.getElementById('testimonials');
    if (!!testimonials) {
        testimonialsSlider.setup();
    
        if (!!('ontouchstart' in window)) {
          var hammer = new Hammer(testimonials);
          hammer.on('swipeleft', function() {
            testimonialsSlider.displayNextSlide('right');
          });
          hammer.on('swiperight', function() {
            testimonialsSlider.displayNextSlide('left');
          });
        }
    }
    
    ///////////////////////// projects slider //////////////////////////////

    var projects = document.getElementById('projects');
    if (!!projects) {
        projectsSlider.setup();
    
        if (!!('ontouchstart' in window)) {
          var hammer = new Hammer(projects);
          hammer.on('swipeleft', function() {
            projectsSlider.displayNextSlide('right');
          });
          hammer.on('swiperight', function() {
            projectsSlider.displayNextSlide('left');
          });
        }
    }

});
//             var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
//     if(!isMobile) {
//  jQuery('.expandable-menu').hover(
//          function(e) {
//             jQuery('.expandable-menu .sub-menu').show();
//         },
//         function (e) {
//             jQuery('.expandable-menu .sub-menu').hide();
//         }
//     )};