var initialImage = 0;
var currentImage = initialImage;
var nImages;

var imageAnimationTimeoutTime = 12000;
var imageAnimationRunning = false;
var enqueuedImageAnimationDirection = null;
var imageSlideTime = scrollAnimationTime / 2000 * 4;

var imageAnimationTimeout;

function hideImages() {
    $('.realisation-image').hide();
}

function setImageAnimationTimeout() {    
    imageAnimationTimeout = setTimeout(function() {
        showImage('right');
    }, imageAnimationTimeoutTime);
}

function clearImageAnimationTimeout() {    
    clearTimeout(imageAnimationTimeout);
    imageAnimationTimeout = null;
}

function imageAnimationCallback() {
    imageAnimationRunning = false;
    if (enqueuedImageAnimationDirection) {
        showImage(enqueuedImageAnimationDirection);
        enqueuedImageAnimationDirection = null;
    } else {
        setImageAnimationTimeout();
    }
}

function showImage(direction, next) {
    imageAnimationRunning = true;
    clearImageAnimationTimeout();
    var previousImage = currentImage;
    if (next !== undefined) {
        currentImage = next;
    } else {
        switch (direction) {
            case 'left' :
                currentImage = (currentImage + nImages - 1) % nImages;
                break;
            case 'right' :
                currentImage = (currentImage + 1) % nImages;
                break;
            default : 
                break;
        }        
    }
    $('#images .section-link').removeClass('selected'); 
    $('#images .section-link[data-index="' + currentImage + '"]').addClass('selected');
    $previousImage = $('.realisation-image[data-index="' + previousImage + '"]');
    $image = $('.realisation-image[data-index="' + currentImage + '"]').show();
    var slideEase = Power4.easeIn;
    var timeline = new TimelineLite({onComplete:imageAnimationCallback});
        if (direction === 'left') {
            timeline.set($image, {left: '-100%', display: 'block'})
                    .to($previousImage, imageSlideTime, {left: '100%', ease: slideEase});
        } else if (direction === 'right') {
            timeline.set($image, {left: '100%', display: 'block'})
                    .to($previousImage, imageSlideTime, {left: '-100%', ease: slideEase});
        }    
        timeline.to($image, imageSlideTime, {left: '0', ease: slideEase}, 0)
                .set($previousImage, {left: '-100%', display: 'none'});
}

function displayImage(direction) {    
    if (!imageAnimationRunning) {
        showImage(direction);
    } else if (!enqueuedImageAnimationDirection) {
        enqueuedImageAnimationDirection = direction;
    }
}

$(document).ready(function() {
    nImages = $('.realisation-image').length;   
    hideImages();
    $('.realisation-image[data-index="' + currentImage + '"]').show();
    setImageAnimationTimeout();
    
    $('#next').click(function() {
        displayImage('right');
    });
    
    $('#previous').click(function() {
        displayImage('left');
    });
    
    if (!!('ontouchstart' in window)) {
        var images = document.getElementById('images');
        var hammer = new Hammer(images);
        hammer.on('swipeleft', function() {
            displayImage('right');
        });
        hammer.on('swiperight', function() {
            displayImage('left');
        });
    }
    
    $('#images .section-link').click(function() {
        var index = $(this).data('index');
        if (index != currentImage) {
            if ((currentImage + nImages - 1) % nImages == index) {
                displayImage('left');
            } else {
                displayImage('right', index);
            }
        }
    });
    
});