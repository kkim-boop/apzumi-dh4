var sloganTopHeight = 100;
var contentTopHeight = 80;
var topHeight = sloganTopHeight;

var scrollPosition = 0;
var scrollThreshold = 2;
var defaultAlignmentThreshold = 2;
var wpAdminBarCorrection = 30;
var sectionAligned = true;

var sections = [];
var currentSectionIndex = 0;

var scrollAnimationTime = 400;
var scrollAnimationTimeout = 100;
var animationRunning = false;

var minScrollInterval = 200;
var lastScrollDirection = 'down';
var lastScrollTop = 0;
var lastScrollEventTime = 0;
var lastScrollEventDelta = 0;
var lastScrollAccelerationTime = 0;
var offset = 0;

var height = 0;
var canPositionResize = true;

var disableScroll = false;
var lastTouchY = null;

function updateHeader(index/*$section*/) {    
    /*if ($section.hasClass('slogan') || $section.parent().hasClass('slogan')) {
        $('#top1').removeClass('top2');
    } else {
        $('#top1').addClass('top2');
    }*/
    $section = $('#' + sections[index]);
    if ($section.hasClass('slogan') || $section.parent().hasClass('slogan') || index === sections.length-1) {
        $('#top1').removeClass('top2');
    } else {
        $('#top1').addClass('top2');
    }
}

function scrollTo(position) {  
        scrollPosition = position;
        animationRunning = true;  
        $('html,body').animate(
            {
                    scrollTop: position
            }, 
            scrollAnimationTime, 'easeInQuart', function () {
                /*setTimeout( function() {
                    animationRunning = false;                    
                }, scrollAnimationTimeout)*/
                animationRunning = false;
            }
        );
}

function handleScroll (e, section) {
    //var top = $(window).scrollTop();
    if (disableScroll) return;

    var delta = 0;
    var scrollDirection = null;
    var width = $(window).width();
    var height = $(window).height();
    
    if (typeof section === "undefined" && width >= 980 && height >= 618) { 
        if ('deltaY' in e) {
            delta = e.deltaY;
        } else if ('wheelDelta' in e) {
            delta = -e.wheelDelta;
        } else if ('keyCode' in e) {
            if (e.keyCode == 40 || e.keyCode == 34) {
                delta = 240;
            } else if (e.keyCode == 38 || e.keyCode == 33) {
                delta = -240;
            }
        } else if ('target' in e) {
            if ($(e.target).hasClass('arrow-down')) {
                delta = 480;
            }
        } 
        if (delta != 0) {
            scrollDirection = delta > 0 ? 'down' : 'up';
        }
    }
    //var minBottom = top + scrollThreshold + height; 
        
    if (sections.length && (scrollDirection !== null || typeof section !== "undefined")) {  
        var scrollReady = !animationRunning;       
            if (!section) { 
                var scrollEventTime = Date.now();
                var scrollInterval = scrollEventTime - lastScrollEventTime;
                var scrollAccelerationInterval = 0;
                if (scrollDirection === lastScrollDirection) {
                    if (delta > lastScrollEventDelta) {
                        scrollAccelerationInterval = scrollEventTime - lastScrollAccelerationTime;
                        lastScrollAccelerationTime = scrollEventTime;            
                        //console.log(scrollAccelerationInterval + ' -- ' + lastScrollAccelerationTime + ' -> ' + scrollEventTime + ' : ' + lastScrollEventDelta + ' -> ' + e.deltaY);
                    }    
                    scrollReady = scrollReady && (scrollInterval >= minScrollInterval || scrollAccelerationInterval >= scrollAnimationTime + scrollAnimationTimeout);
                } else {
                    lastScrollAccelerationTime = scrollEventTime;
                }
                //console.log(scrollInterval + ' -- ' + lastScrollEventTime + ' -> ' + scrollEventTime + ' : ' + e.deltaY);
                lastScrollEventDelta = delta;
                lastScrollEventTime = scrollEventTime;
                lastScrollDirection = scrollDirection;
            }
            if (scrollReady) {           
                scrollPosition = 0;
                var scrollToFooter = false;
                if (!section) {
                    var sectionIndex = currentSectionIndex;
                    if (scrollDirection === 'down') {
                        if (currentSectionIndex > 0) {
                            sectionIndex = currentSectionIndex - 1;                            
                        } else {
                            scrollToFooter = true;
                        }
                    } else if (scrollDirection === 'up' && sectionAligned && currentSectionIndex < sections.length-1) {
                        sectionIndex = currentSectionIndex + 1;
                    }
                    section = sections[sectionIndex];
                }
                if (scrollToFooter) {
                    var $footer = $('#footer');
                    scrollPosition = $footer.position().top + $footer.height();
                } else {                    
                    var $section = $('#' + section);
                    scrollPosition = $section.position().top;
                    if (scrollPosition === 0) {
                        scrollPosition -= 100;
                    } else {
                        scrollPosition -= 80;
                    }
                }  
                
                /*for (var i in sections) {
                    var $section = $('#' + sections[i]);
                    var sectionTop = $section.position().top;
                    var sectionBottom = sectionTop + height;
                    if (!section && i == 0 && minBottom >= sectionBottom) {
                        scrollPosition = $('#footer').position().top + $('#footer').height();
                        break;
                    } else if ((section && section === sections[i]) || !section && (minBottom > sectionTop)) {
                        scrollPosition = sectionTop;                        
                        section = sections[i];
                        currentSectionIndex = i;
                        break;    
                    }
                }*/
                scrollTo(scrollPosition);      
                if (section && typeof resetSection === 'function') {
                    resetSection(section);
                }
        }
        return false;
    }
}

function setCurrentSectionIndex() {
    var top = $(window).scrollTop();
    var headerUpdated = false;
    var sectionIndexUpdated = false;
    for (var i in sections) {
        var index = parseInt(i);
        var $section = $('#' + sections[index]);
        var sectionTop = $section.position().top;
        if (!headerUpdated && sectionTop <= top + topHeight) {
            if ($(window).width() <= 979 || $('body').hasClass('mobile')) {
                if (top === 0 && !$section.hasClass('realisation-hero')) { 
                    $('#top1').removeClass('top2');
                } else {
                    $('#top1').addClass('top2');
                } 
            } else {                
                updateHeader(index);
            }
            headerUpdated = true;
        }
        var alignmentThreshold = defaultAlignmentThreshold;
        //WP Admin Bar Fix
        if (document.getElementById('wpadminbar')) {
            alignmentThreshold += wpAdminBarCorrection;
        }
        //End Fix
        if (!sectionIndexUpdated && (sectionTop < top + 80 + alignmentThreshold)) {
            sectionIndexUpdated = true;
            currentSectionIndex = index;
            offset = top - sectionTop + 80;
            sectionAligned = Math.abs(offset) <= alignmentThreshold;
            offsetPercentage = offset === 0 ? 0 : offset / $(window).height();
        }
        if (sectionIndexUpdated && headerUpdated) {
            break;
        }
    }
}

function goToHashId(hash) {
            var id = hash.substr(1);
            if (document.getElementById(id)) {
                handleScroll({}, id);
            }    
}

$(document).ready(function() {    
        lastScrollTop = $(window).scrollTop();
        height = $(window).height();
        scrollPosition = lastScrollTop;

        $sections = $(".section").get();
        sectionsLength = $sections.length

        if (sectionsLength == 0) {
            $('#top1').addClass('top2');
        }

        $($sections.reverse()).each(function () {
            sections.push($(this).attr('id'));
        });
                
        setCurrentSectionIndex();
                
        $(window).scroll(function () {
            setCurrentSectionIndex();
            lastScrollTop = $(window).scrollTop();
        });
        
        if(window.location.hash) {
            goToHashId(window.location.hash);
        }

        $(window).bind('hashchange', function() {
            goToHashId(window.location.hash);
        });
                
        /*$(document).bind('touchmove', function (e){
             var currentTouchY = e.originalEvent.touches[0].clientY;
             if(!!lastTouchY && currentTouchY > lastTouchY && $(window).scrollTop() <= 0){
             }
             lastTouchY = currentTouchY;
        });*/
        
    if(!$('body').hasClass('std-scroll')) {            
        $('.bs-privacy-modal').on('show.bs.modal',function(e){
            disableScroll=true;
        });

        $('.bs-privacy-modal').on('hide.bs.modal',function(e){
            disableScroll=false;
        });

            if (window.addEventListener) {
                window.addEventListener('DOMMouseScroll', handleScroll, false);
            }
            window.onwheel = handleScroll;
            window.onmousewheel = document.onmousewheel = handleScroll;
            document.onkeydown = handleScroll;
            //window.ontouchmove  = handleTouch //TODO MAYBE;
            $('.arrow-down').click(function (e) {
                if (currentSectionIndex !== 0 || $(window).width() <= 980) {
                    handleScroll(e, sections[currentSectionIndex - 1]);
                }
            });
            
    } else {
        disableScroll = true;
    }
});
