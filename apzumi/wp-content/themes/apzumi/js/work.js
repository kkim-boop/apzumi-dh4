var $realisationLinks;
var nRealisationLinks;

function setLinksHeight($container, $links) {
    var height = $container.height();
    var nLinks = $links.length;
    var linkHeight = height / nLinks;
    $links.each( function() {
        $(this).css('height', linkHeight);
    });
}

$(document).ready(function() {
    var $container = $('#our-work');
    if($container) {
        var $links = $('#our-work > a');

        setLinksHeight($container, $links);

        $(window).resize(function () {
            setLinksHeight($container, $links);
        });

        $links.hover(
            function () {
                var realisation = $(this).data('realisation');
                var bgClass = 'our-work-' + realisation + '-bg';
                $('#top1').addClass(bgClass);
                $(this).parent().addClass(bgClass);
            },
            function () {
                var realisation = $(this).data('realisation');
                var bgClass = 'our-work-' + realisation + '-bg';
                $('#top1').removeClass(bgClass);
                $(this).parent().removeClass(bgClass);
            })
    }
});

function setBlogLinksHeight( containerHeight, $links) {
    var viewportHeight = $( window ).height();
    if(viewportHeight > containerHeight) {
        var nLinks = 3;
        var height = viewportHeight - $('#top1').outerHeight();
        var linkHeight = height / nLinks;
        $links.each( function() {
            $(this).css('height', linkHeight);
        });
    } else {
        $links.each( function() {
            $(this).css('height', '');
        });
    }
}

$(document).ready(function() {
    var $container = $('#blog-posts');
    if ($container) {
        var $links = $('#blog-posts > a');

        var containerHeight = $container.outerHeight();
        setBlogLinksHeight(containerHeight, $links);

        $(window).resize(function () {
            setBlogLinksHeight(containerHeight, $links);
        });

        $links.hover(
            function () {
                var bgUrl = $(this).data('featured-image');
                $('#top1').attr('style', 'background-image: url(' + bgUrl + ') !important;');
                $(this).parent().attr('style', 'background-image: url(' + bgUrl + ') !important;');

                $('#top1').addClass('blog-posts-highlight-bg-color');
                $(this).parent().addClass('blog-posts-highlight-bg-color');
            },
            function () {
                $('#top1').css('background-image', 'none');
                $(this).parent().css('background-image', 'none');

                $('#top1').removeClass('blog-posts-highlight-bg-color');
                $(this).parent().removeClass('blog-posts-highlight-bg-color');
            })

        $('#career-join-us-up').click(function (e) {
            e.preventDefault();
            if (typeof handleScroll === 'function') {
                handleScroll(e, "job-offers");
            }
        });
    }
});