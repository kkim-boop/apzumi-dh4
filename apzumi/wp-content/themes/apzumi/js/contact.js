var map, marker;
var usaLatln =new google.maps.LatLng(35.995769, -78.902138);
var polandLatln =new google.maps.LatLng(52.405684,16.9155099);
var germanyLatln =new google.maps.LatLng(50.1112821, 8.6525775);

function initialize() {
    var mapCanvas = document.getElementById("map_canvas");
    if (!mapCanvas) {
        return;
    }

    var myOptions = {
        zoom: 15,
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        scrollwheel: false,
        disableDefaultUI: true,
        center: usaLatln,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var styleArray = [
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#717dcb"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#253177"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#253177"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#253177"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "lightness": 21
                },
                {
                    "color": "#28347f"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#39437f"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#7a82b4"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#39437e"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#253177"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#1e275f"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ];
    map = new google.maps.Map(mapCanvas, myOptions);
    map.setOptions({styles: styleArray});



    var image = themeUrl + '/img/marker.png';
    marker = new google.maps.Marker({
        position: usaLatln,
        title:"apzumi",
        icon: image
    });
    marker.setMap(map);


    var rectOptions = {
        strokeColor: '#071153',
        strokeOpacity: 0.6,
        strokeWeight: 2,
        fillColor: '#071153',
        fillOpacity: 0.7,
        map: map,
        bounds: new google.maps.LatLngBounds(
            new google.maps.LatLng(-90, -180),
            new google.maps.LatLng(90, 180))
    };

    //var rectangle = new google.maps.Rectangle(rectOptions);

    if (typeof getUrlParameter === 'function') {
        var location = getUrlParameter('location');
        if (location) {
            if (location === 'poland') {
                showPoland();
            } else if (location === 'usa') {
                showUSA();
            } else if (location === 'germany') {
                showGermany();
            }
            if (typeof handleScroll === 'function') {
                handleScroll({}, 'addresses');
            }
        }
    }
}
google.maps.event.addDomListener(window, "load", initialize);

function showPoland () {
    $('#addresses .address').eq(1).addClass('active');
    $('#addresses .address').eq(0).removeClass('active');
    $('#addresses .address').eq(2).removeClass('active');
    map.panTo(polandLatln);
    marker.setPosition(polandLatln);
    $('#map .usa').hide();
    $('#map .germany').hide();
    $('#map .poland').show();
}

function showUSA () {
    $('#addresses .address').eq(0).addClass('active');
    $('#addresses .address').eq(1).removeClass('active');
    $('#addresses .address').eq(2).removeClass('active');
    map.panTo(usaLatln);
    marker.setPosition(usaLatln);
    $('#map .poland').hide();
    $('#map .germany').hide();
    $('#map .usa').show();
}

function showGermany () {
    $('#addresses .address').eq(2).addClass('active');
    $('#addresses .address').eq(0).removeClass('active');
    $('#addresses .address').eq(1).removeClass('active');
    map.panTo(germanyLatln);
    marker.setPosition(germanyLatln);
    $('#map .poland').hide();
    $('#map .usa').hide();
    $('#map .germany').show();
}
