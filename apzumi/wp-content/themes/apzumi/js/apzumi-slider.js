var apzumiSlider = function(args) {

    this.sliderContainerId              = args.sliderContainerId;
    this.initialSlide                   = args.initialSlide;
    this.slideSelector                  = args.slideSelector;
    this.numberedSlideSelectorFunction  = args.numberedSlideSelectorFunction;
    this.nSlides                        = args.nSlides;
    this.currentSlide                   = args.currentSlide;
    this.slideAnimationTimeoutTime      = args.slideAnimationTimeoutTime;
    this.slideTime                      = args.slideTime;
    this.slideAnimationRunning          = false;
    this.targetDirection                = null;
    this.animationTimeout               = null;
    this.autoSlide                      = (args.autoSlide === true) ? true : false;
    this.targetSlide                    = 0;
    if(args.arrows) {
        this.arrows = args.arrows;
    } else {
        this.arrows = {
            previous: '.arrow-previous',
            next: '.arrow-next',
            back: '.arrow-back',
        }
    }

    if (args.innerSlideAnimationCallback) {
        this.innerSlideAnimationCallback = args.innerSlideAnimationCallback;
    } else {
        this.innerSlideAnimationCallback = function($slide, direction, timeline, delay) {};
    }

    this.setup = function() {
        $sliderContainer = $(this.sliderContainerId);
        if ($sliderContainer.length == 0) {
            return;
        }
        this.hideSlides();
        $(this.sliderContainerId + ' ' + this.numberedSlideSelectorFunction(this.initialSlide)).show();
        $(this.sliderContainerId + ' .section-link[data-index="' + this.initialSlide + '"]').addClass('selected');

        if(this.autoSlide) {
            this.setAnimationTimeout();
        }

        $(this.sliderContainerId + ' .section-link').click(function (event) {
            var index = $(event.delegateTarget).data('index');
            if (index != this.currentSlide) {
                skipSize = index - this.currentSlide;
                if (skipSize > 0 && skipSize <= this.nSlides / 2 || skipSize < 0 && -skipSize >= this.nSlides / 2)  {
                    direction = 'right';
                } else {
                    direction = 'left';
                }
                this.targetDirection = direction;
                this.targetSlide = index;
                this.displaySlide(direction);
            }
        }.bind(this));

        $(this.sliderContainerId  + ' ' + this.arrows.next).click(function () {
            this.displayNextSlide('right');
        }.bind(this));

        $(this.sliderContainerId  + ' ' + this.arrows.previous).click(function () {
            this.displayNextSlide('left');
        }.bind(this));

        $(this.sliderContainerId  + ' ' + this.arrows.back).click(function () {
            this.displaySlide('down');
        }.bind(this));
    }

    this.hideSlides = function() {
        $slides = $(this.sliderContainerId + ' ' + this.slideSelector);
        $slides.hide();
    };

    this.slideAnimationCallback = function() {
        this.slideAnimationRunning = false;
        if (this.targetSlide != this.currentSlide) {
            this.displaySlide(this.targetDirection, true);
        } else if (this.autoSlide) {
            this.setAnimationTimeout();
        }
    };

    this.displayNextSlide = function(direction) {        
        this.targetSlide = this.getNextSlideIndex(direction);
        this.targetDirection = direction
        this.displaySlide(direction);
    }

    this.displaySlide = function(direction, continuous = false) {
        if (!this.slideAnimationRunning) {
            this.slideAnimationRunning = true;
            this.clearAnimationTimeout();
            var previousSlide = this.currentSlide;
            this.currentSlide = this.getNextSlideIndex(direction);
            if (direction == 'down') {
                this.hideSlides();
            }

            $(this.sliderContainerId + ' .section-link').removeClass('selected');
            $(this.sliderContainerId + ' .section-link[data-index="' + this.currentSlide + '"]').addClass('selected');
            $previousSlide = $(this.sliderContainerId + ' ' + this.numberedSlideSelectorFunction(previousSlide));
            $slide = $(this.sliderContainerId  + ' ' + this.numberedSlideSelectorFunction(this.currentSlide)).show();

            var slideEase = Power4.easeIn;
            var timeline = new TimelineLite({onComplete: this.slideAnimationCallback.bind(this) });
            var delay;
            if (direction === 'down') {
                delay = scrollAnimationTime / 2000 * 2;
            } else {
                delay = this.slideTime;
                if (direction === 'left') {
                    timeline.set($slide, {left: '-100%', display: 'block'})
                        .to($previousSlide, this.slideTime, {left: '100%', ease: slideEase});
                } else if (direction === 'right') {
                    timeline.set($slide, {left: '100%', display: 'block'})
                        .to($previousSlide, this.slideTime, {left: '-100%', ease: slideEase});
                }
                timeline.to($slide, this.slideTime, {left: '0', ease: slideEase}, 0)
                    .set($previousSlide, {left: '0', display: 'none'});
            }
            this.innerSlideAnimationCallback($slide, direction, timeline, delay);
        }
    };

    this.setAnimationTimeout = function() {
        var showCallback = function() {
            this.displayNextSlide('right');
        };
        this.animationTimeout = setTimeout(showCallback.bind(this), this.slideAnimationTimeoutTime);
    };

    this.clearAnimationTimeout = function() {
        clearTimeout(this.animationTimeout);
        this.animationTimeout = null;
    };

    this.getNextSlideIndex = function(direction) {
        if (direction == 'right') {
            nextSlideIndex = (this.currentSlide + 1) % this.nSlides;
        } else if (direction == 'left') {
            nextSlideIndex = (this.currentSlide + this.nSlides - 1) % this.nSlides;
        } else {
            nextSlideIndex = this.initialSlide;
        }
        return nextSlideIndex;
    }

};