<?php get_header();?>

<div id="product-strategy">

    <div id="it-consulting-hero" class="section flex slogan" <?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
        <div class="container">
            <h1>Remove all uncertainty</h1>
            <h3>when our committed team of experienced and energetic advisors back up your ideas, help build your business model and to refine your MVP.</h3>
        </div>
        <img src="<?php echo apzumi_image_directory()?>/arrow-down.png" class="arrow-down">
    </div>

    <div class="section-title">We specialize in</div>

    <div id="step1" class="section flex step">
        <div class="container padded flex space-between default-width">
            <img class="img-left no-mobile" src="<?php echo apzumi_image_directory()?>/steps/step1.png">
            <div class="step-text desktop-pull-right">
                <div class="no">
                    01
                </div>
                <h1>Business Concept Development</h1>
                <p>
                    We learn the unique character of your idea and help you
                    to define all technical requirements. Through detailed
                    analyses of your needs, we recommend you a software
                    solution that fits your project the best. We develop
                    a prototype or a MVP in a trustworthy manner that saves
                    your time and money.
                </p>
                <img class="only-mobile" src="<?php echo apzumi_image_directory()?>/steps/step1.png">
                <p>
                    Our participation in the project shall optimize your burn
                    ratio and enable you to build product attractive for
                    investors and customers.
                </p>
            </div>
        </div>
    </div>

    <div id="step2" class="section flex step">
        <div class="container padded flex space-between default-width">
            <div class="step-text desktop-pull-left">
                <div class="no">
                    02
                </div>
                <h1>Product Development</h1>
                <p>
                    We help you to develop your product quickly and
                    methodically, using broad experience and industry knowledge.
                    We also help you in product launch and commercialization.
                </p>
                <img class="only-mobile" src="<?php echo apzumi_image_directory()?>/steps/step2.png">
                <p>
                    Applied design and usability shall delight your customers.
                    If you are satisfied with our services you shall contract us
                    to ensure your internet performance is
                    further growing and expanding.
                </p>
            </div>
            <img class="img-right no-mobile" src="<?php echo apzumi_image_directory()?>/steps/step2.png">
        </div>
    </div>

    <div id="step3" class="section flex step">
        <div class="container padded flex space-between default-width padded">
            <img class="img-left no-mobile" src="<?php echo apzumi_image_directory()?>/steps/step3.png">
            <div class="step-text desktop-pull-right">
                <div class="no">
                    03
                </div>
                <h1>IT <span class="nobr">policy and</span> strategy</h1>
                <p>
                    We support you to prepare a comprehensive IT management
                    plan for your entire organization. The plan will be created in accordance with your
                    <strong>business concept, strategy and business priorities.</strong>
                </p>
                <img class="only-mobile" src="<?php echo apzumi_image_directory()?>/steps/step3.png">
                <p>
                    Our role is to ensure that you shall capitalize on use of the most
                    modern technologies. Inspecting business processes and functions
                    shall result in optimizing use of technology in your future expanding operations.
                    The IT policy and strategy shall create added value for the organization and
                    ensure seamless growth going well beyond present time.
                </p>
            </div>
        </div>
    </div>

    <a class="full-width-button button button-red no-fixed" href="/contact">
        <span>LET'S TALK</span>
        <span class="right-arrow"></span>
    </a>

</div>

<?php
get_footer();
