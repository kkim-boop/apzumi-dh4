<?php get_header();?>
<div id="career">
    <div id="career-banner" class="section" data-offset="0">
        <div class="flex image-container image-1 career-left-bg">
            <h1>Innovators <span class="nobr">and good</span> friends</h1>
        </div>
        <div class="flex image-container image-2 career-right-top-bg">
            <h1>Challengers <span class="nobr">and risk</span> takers</h1>
        </div>
        <div class="flex image-container image-3 career-right-bottom-bg">
            <h1>Flow Evangelists</h1>
        </div>
    </div>
    <div id="job-offers" class="section flex" data-offset="1">
        <div class="container">
            <h1>
                Become a part of our story as:
            </h1>
            <table>
                <tr>
                    <th>Java Developer</th>
                    <td class="employment-type">Full time</td>
                    <td class="city">Poznań</td>
                    <td class="apply">
                        <div class="offer-btn">
                            <span class="offer-btn-text">Apply</span> <img src="<?php echo apzumi_image_directory()?>/arror-right-red.png">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>PHP Developer</th>
                    <td>Full time</td>
                    <td>Poznań</td>
                    <td>
                        <div class="offer-btn">
                            <span class="offer-btn-text">Apply</span> <img src="<?php echo apzumi_image_directory()?>/arror-right-red.png">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Project Manager</th>
                    <td>Part time</td>
                    <td>Freelance</td>
                    <td>
                        <div class="offer-btn">
                            <span class="offer-btn-text">Apply</span> <img src="<?php echo apzumi_image_directory()?>/arror-right-red.png">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="challenge-yourself" class="section flex" data-offset="2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Challenge yourself</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/learn_teach.svg" alt="Learn & teach others">
                    <h2>Learn & teach<br>others</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/impact_startup_industry.svg" alt="Impact the startup industry">
                    <h2>Impact the startup<br>industry</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/fast_scaling_projects.svg" alt="Be the part of fast scaling projects">
                    <h2>Be the part<br> of fast scaling projects</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/develop_your_passions.svg" alt="Develop your passions">
                    <h2>Develop<br>your passions</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory()?>/career/be_yourself.svg" alt="Be yourself. Unique and meaningfull">
                    <h2>Be yourself. Unique and<br>meaningfull</h2>
                </div>
                <div class="col-xs-12 col-md-4 col-item">
                    <img src="<?php echo apzumi_image_directory() ?>/career/inspire_get_inspired.svg" alt="Inspire & get inspired">
                    <h2>Inspire &<br>get inspired</h2>
                </div>
            </div>
        </div>
    </div>
    <div id="work-at-apzumi" class="section flex career-work-at-apzumi-bg" data-offset="3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>What it is like to work at Apzumi</h1>
                    <video id="what-is-it-like-video" poster="<?php echo apzumi_image_directory()?>/video_default.jpg"  controls>
                        <source src="<?php echo apzumi_image_directory()?>/main_video.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <img class="video-overlay-video-btn" src="<?php echo apzumi_image_directory()?>/videoplayer.svg">
                </div>
            </div>
        </div>
    </div>
    <div id="we-offer" class="section flex" data-offset="4">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>We offer</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-offset-1 col-md-5">
                    <ul>
                        <li><p>high salary</p></li>
                        <li><p>integration trips and meetings</p></li>
                        <li><p>mentorship period</p></li>
                        <li><p>strong promotion opportunities</p></li>
                        <li><p>great atmosphere</p></li>
                        <li><p>project variety</p></li>
                        <li><p>health insurance</p></li>
                        <li><p>ambitious & skilled people who share knowledge</p></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-5">
                    <ul>
                        <li><p>daily diet subsidies</p></li>
                        <li><p>daily snacks</p></li>
                        <li><p>agile environment</p></li>
                        <li><p>flexible work hours</p></li>
                        <li><p>MultiSport Card</p></li>
                        <li><p>professional equipment</p></li>
                        <li><p>challenges and contests with prizes</p></li>
                        <li><p>learning resources<br>(books courses)</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="join-us" class="section flex no-fixed">
        <div class="container padded">
            <h1>Sollutions for people made by people.<br>Above than average.</h1>
            <a id="career-join-us-up" href="#" class="contact-btn">
                Join Us <img src="<?php echo apzumi_image_directory()?>/arrow-up-blue.png">
            </a>
        </div>
    </div>
</div>

<?php
get_footer();
