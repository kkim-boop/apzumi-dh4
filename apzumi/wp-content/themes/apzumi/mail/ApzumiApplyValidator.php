<?php
require_once('../recaptcha/autoload.php');
require_once('../recaptcha/config.php');

class ApzumiApplyValidator {
    public $fieldValidator=[];
    public $errors=[];

    public function __construct($fieldValidator)
    {
        $this->fieldValidator = $fieldValidator;
    }

    public function validate($post, $files=null)
    {
        $correct = true;
        foreach ($this->fieldValidator as $validatorData) {

            $func = $validatorData['validator'] . 'Validate';
            $required = (isset($validatorData['required']) && $validatorData['required']===true) ? true : false;

            if(is_array($validatorData['messages'])) {
                $messages = $validatorData['messages'];
            } else {
                $messages = null;
            }

            if(isset($validatorData['type']) && $validatorData['type']==='file'){
                $value = (isset($files[$validatorData['field']])) ? $files[$validatorData['field']] : null;
                $correct = $this->$func($validatorData['field'], $value, $required, $messages) && $correct;
            } else {
                $value = (isset($post[$validatorData['field']])) ? $post[$validatorData['field']] : null;
                $correct = $this->$func($validatorData['field'], $value, $required, $messages) && $correct;
            }

        }

        return $correct;
    }



    public function nameValidate($fieldName, $value, $required, $messages)
    {
        $messages = $this->setMessages(
            [
                'default' => 'Incorrect field',
                'required' => 'Field is required',
            ],
            $messages
        );
        $message = null;

        $valid = true;
        if($required && empty($value)) {
            $message = $messages['required'];
            $valid = false;
        }
        $this->addError($valid, $fieldName, $message);
        return $valid;
    }

    public function longtextValidate($fieldName, $value, $required, $messages)
    {
        $messages = $this->setMessages(
            [
                'default' => 'Incorrect field',
                'required' => 'Field is required',
            ],
            $messages
        );
        $message = null;

        $valid = true;
        if($required && empty($value)) {
            $message = $messages['required'];
            $valid = false;
        }
        $this->addError($valid, $fieldName, $message);
        return $valid;
    }

    public function offerValidate($fieldName, $value, $required, $messages)
    {
        $messages = $this->setMessages(
            [
                'default' => 'Incorrect field',
                'required' => 'Field is required',
            ],
            $messages
        );
        $message = null;

        $valid = true;
        if($required && empty($value)) {
            $message = $messages['required'];
            $valid = false;
        }

        $this->addError($valid, $fieldName, $message);
        return $valid;
    }

    public function emailValidate($fieldName, $value, $required, $messages)
    {
        $messages = $this->setMessages(
            [
                'default' => 'Incorrect e-mail',
                'required' => 'E-mail is required',
            ],
            $messages
        );
        $message = null;

        $valid = true;
        if($required) {
            if(empty($value)) {
                $message = $messages['required'];
                $valid = false;
            } elseif(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $message = $messages['default'];
                $valid = false;
            }
        }

        $this->addError($valid, $fieldName, $message, $required);
        return $valid;
    }

    public function fileValidate($fieldName, $fileData, $required, $messages)
    {
        $messages = $this->setMessages(
            [
                'default'       => 'File error',
                'required'      => 'File is required',
                'upload_error'  => 'File upload error',
                'too_big'       => 'File too big. Maximum file size is 8MB',
            ],
            $messages
        );
        $message = null;

        $valid = true;
        if ( $required && isset($fileData['tmp_name']) && (!file_exists($fileData['tmp_name']) || !is_uploaded_file($fileData['tmp_name']))  ) {
            $message = $messages['required'];
            $valid = false;
        } elseif (!isset($fileData['error']) || is_array($fileData['error'])) {
            $message = $messages['upload_error'];
            $valid = false;
        } elseif ($fileData['size'] > (8 * 1024 * 1024)) {
            $message = $messages['too_big'];
            $valid = false;
        }

        $this->addError($valid, $fieldName, $message);
        return $valid;
    }

    public function recaptchaValidate($fieldName, $value, $required, $messages)
    {
        $messages = $this->setMessages(
            [
                'default'       => 'Incorrect recaptcha',
                'required'      => 'Captcha is required',
            ],
            $messages
        );
        $message = null;

        $valid = true;
        if (empty($value)) {
            $message = $messages['required'];
            $valid = false;
        } else {

            $secret = $GLOBALS['apzumiRecaptchaConfig']['secret'];
            $reCaptcha = new \ReCaptcha\ReCaptcha($secret);

            $captchaResponse = $reCaptcha->verify(
                $value,
                $_SERVER["REMOTE_ADDR"]
            );

            if (!$captchaResponse->isSuccess()) {
                $message = $messages['default'];
                $valid = false;
            }
        }
        $this->addError($valid, $fieldName, $message);
        return $valid;
    }

    /**
     * Helper method to set validator messages to validator
     *
     * @param $default - array of default validator messages
     * @param $custom - array of validator messages set during validation setup
     *
     * @return array - array of final validator messages
     */
    private function setMessages($default, $custom) {
        $messages = $default;
        foreach($default as $messageId => $message) {
            if(isset($custom[$messageId])) {
                $messages[$messageId] = $custom[$messageId];
            }
        }
        return $messages;
    }

    /**
     * Helper method to set error for field
     *
     * @param boolean $valid
     * @param $fieldName
     * @param $errorMessage
     */
    private function addError($valid, $fieldName, $errorMessage)
    {
        if(!$valid && !isset($this->errors[$fieldName])) {
            $this->errors[$fieldName] = $errorMessage;
        }
    }
}