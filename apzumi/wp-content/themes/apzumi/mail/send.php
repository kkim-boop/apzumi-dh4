<?php

// CONFIG
require_once('config.php');

if($_POST['body'] || $_POST['subscription-email'] || $_POST['send_cv']) {

    require dirname(__FILE__).DIRECTORY_SEPARATOR.'PHPMailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    if($_POST['body']) {
        $body = $_POST['body'];
        $bodyNlToBr = nl2br($body);
        $firstName = $_POST['firstName'];
        $surname = $_POST['surname'];
        $email = $_POST['email'];
        $company = $_POST['company'];

        $mail->Subject = $contact['subject'];
        $mail->Body    = <<<EOT
        <strong>Name</strong>: $firstName<br>
        <strong>Surname</strong>: $surname<br>
        <strong>E-mail</strong>: $email<br>
        <strong>Company</strong>: $company<br>
        <strong>Treść</strong>:<br>$bodyNlToBr
EOT;
        $mail->AltBody = <<<EOT2
                Name: $firstName
                Surname: $surname
                E-mail: $email
                Company: $company
                Treść:
                $body
EOT2;

    } elseif($_POST['send_cv']) {

        if (isset($_FILES['uploaded_file']) && $_FILES['uploaded_file']['error'] == UPLOAD_ERR_OK) {

            $mail->AddAttachment($_FILES['uploaded_file']['tmp_name'],$_FILES['uploaded_file']['name']);

            $offer = $_POST['offer'];
            $name = $_POST['name'];
            $email = $_POST['email'];

            $mail->Subject = 'Apzumi - CV from ' . $name;
            $mail->Body    = <<<EOT
            <strong>Offer</strong>: $offer<br>
            <strong>Name</strong>: $name<br>
            <strong>E-mail</strong>: $email<br>
EOT;
            $mail->AltBody = <<<EOT2
                Offer: $offer
                Name: $name
                E-mail: $email
EOT2;

        } else {
            echo 'No file supplied';
            exit;
        }


    } else {
        $subscriptionEmail = $_POST['subscription-email'];
        $mail->Subject = $contact['subject'];
    $mail->Body    = <<<EOT
    <strong>Subscription e-mail</strong>: $subscriptionEmail<br>
EOT;

    $mail->AltBody = <<<EOT2
            Subscription e-mail: $subscriptionEmail
EOT2;
    }

    


    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $host;  // Specify main and backup SMTP servers
    $mail->SMTPAuth = $smtpAuth; 
    $mail->SMTPDebug = false;
    // Enable SMTP authentication
    $mail->Username = $username;                 // SMTP username
    $mail->Password = $password;                           // SMTP password
    $mail->CharSet = 'UTF-8';
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $port;                                    // TCP port to connect to


//$mail->isSendmail();

$mail->From = $contact['from'];
$mail->FromName = $contact['fromName'];
    if(in_array($_POST['email'], [
        'rafal.sobczyn@gmail.com',
        'andrzej.krajna@enterprisesoftware.pl',
    ])){
        $mail->addAddress($_POST['email']);               // Name is optional
    } else {
        $mail->addAddress($contact['to']);               // Name is optional
    }

$mail->addReplyTo($contact['from'], $contact['fromName']);

$mail->isHTML(true);                                  // Set email format to HTML

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'OK';
    }

} else {
    echo 'Message could not be sent. Empty body.';
}
