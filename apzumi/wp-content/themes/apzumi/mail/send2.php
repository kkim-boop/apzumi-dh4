<?php
//require_once('../recaptcha/autoload.php');
//require_once('../recaptcha/config.php');

require_once('ApzumiApplyValidator.php');


$response = [
    'status' => 'ok',
    'errors' => null
];

$messages = [
    'firstName'             => ['default'=>'Name is incorrect', 'required'=>'Name is required'],
    'surname'               => ['default'=>'Surname is incorrect', 'required'=>'Surname is required'],
    'company'               => ['default'=>'Company is incorrect', 'required'=>'Company is required'],
    'body'                  => ['default'=>'Message is incorrect', 'required'=>'Message is required'],
];

$validatorsData = [
    ['field'=>'firstName',               'validator'=>'name', 'required'=>true, 'messages'=>$messages['firstName']],
    ['field'=>'surname',                 'validator'=>'name', 'required'=>true, 'messages'=>$messages['surname']],
    ['field'=>'company',                 'validator'=>'name', 'required'=>true, 'messages'=>$messages['company']],
    ['field'=>'body',                    'validator'=>'longtext', 'required'=>true, 'messages'=>$messages['body']],
    ['field'=>'email',                   'validator'=>'email', 'required'=>true],
    ['field'=>'g-recaptcha-response',    'validator'=>'recaptcha', 'required'=>true],
];
$validator = new ApzumiApplyValidator($validatorsData);

if($validator->validate($_POST)) {

    $url = 'http://apzumi.enterprisesoftware.pl/wp-content/themes/apzumi/mail/send.php';
    $data = array(
        'firstName' => $_POST['firstName'],
        'surname' => $_POST['surname'],
        'company' => $_POST['company'],
        'email' => $_POST['email'],
        'body' => $_POST['body']
    );

    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        ),
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    if($result !== 'OK') {
        $response['status'] = 'email_error';
        $response['errors'] = $result;
    }

} else {
    $response['status'] = 'error';
    $response['errors'] = $validator->errors;
}

echo(json_encode($response));


exit;

$secret = $GLOBALS['apzumiRecaptchaConfig']['secret'];
$reCaptcha = new \ReCaptcha\ReCaptcha($secret);

$captchaResponse = $reCaptcha->verify(
    $_POST['g-recaptcha-response'],
    $_SERVER["REMOTE_ADDR"]
);

if ($captchaResponse->isSuccess()) {

    $url = 'http://apzumi.enterprisesoftware.pl/wp-content/themes/apzumi/mail/send.php';
    $data = array(
        'firstName' => $_POST['firstName'],
        'surname' => $_POST['surname'],
        'company' => $_POST['company'],
        'email' => $_POST['email'],
        'body' => $_POST['body']
    );

    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        ),
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    print_r($result);

} else {
    echo('wrong_captcha');
}



