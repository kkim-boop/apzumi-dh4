<?php
require_once('ApzumiApplyValidator.php');

function createContent($data) {
    $multipartBoudary = '----------------------------' . microtime(true);

    $content =  "--".$multipartBoudary."\r\n".
        "Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"".basename($data['file']['name'])."\"\r\n".
        "Content-Type: application/octet-stream\r\n\r\n".
        file_get_contents($data['file']['tmp_name'])."\r\n";

    $content .= "--" . $multipartBoudary . "\r\n" . "Content-Disposition: form-data; name=\"send_cv\"\r\n\r\ntrue\r\n";
    foreach($data as $key => $value) {
        $content .= "--" . $multipartBoudary . "\r\n" . "Content-Disposition: form-data; name=\"".$key."\"\r\n\r\n" . $value . "\r\n";
    }

    // signal end of request (note the trailing "--")
    $content .= "--".$multipartBoudary."--\r\n";

    $header = 'Content-Type: multipart/form-data; boundary=' . $multipartBoudary;
    return ['header'=>$header, 'content'=>$content];
}


/////////// begin ///////////
$response = [
    'status' => 'ok',
    'errors' => null
];

$messages = [
    'name'          => ['default'=>'Name is incorrect', 'required'=>'Name is required'],
    'uploaded_cv'   => [
        'default'       => 'CV file error',
        'required'      => 'CV file is required',
        'upload_error'  => 'CV file upload error',
        'too_big'       => 'CV file is too big. Maximum file size is 8MB',
    ]
];

$validatorsData = [
    ['field'=>'offer',                  'validator'=>'offer', 'required'=>false],
    ['field'=>'name',                   'validator'=>'name', 'required'=>true, 'messages' => $messages['name']],
    ['field'=>'email',                  'validator'=>'email', 'required'=>true],
    ['field'=>'uploaded_cv',            'validator'=>'file', 'type'=>'file', 'required'=>true, 'messages' => $messages['uploaded_cv']],
    ['field'=>'g-recaptcha-response',   'validator'=>'recaptcha', 'required'=>true],
];

$validator = new ApzumiApplyValidator($validatorsData);

if($validator->validate($_POST, $_FILES)) {
    $url = 'http://apzumi.enterprisesoftware.pl/wp-content/themes/apzumi/mail/send.php';

    $data = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'offer' => $_POST['offer'],
        'file' => $_FILES['uploaded_cv']
    );

    $emailData = createContent($data);
    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => $emailData['header'],
            'method'  => 'POST',
            'content' => $emailData['content'],
        ),
    );

    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    if($result !== 'OK') {
        $response['status'] = 'email_error';
        $response['errors'] = $result;
    }

} else {
    $response['status'] = 'error';
    $response['errors'] = $validator->errors;
}

echo(json_encode($response));