<?php
/**
 * Class JobOfferFieldGroup
 */
class JobOfferFieldGroup {
    public $fieldGroup, $tableType, $fields, $displayBarSumValues;
    private $tableName;

    public function __construct($fieldGroup, $tableType)
    {
        $this->fieldGroup = $fieldGroup;
        $this->fields = $fieldGroup['fields'];
        if(is_array($tableType) ){
            $this->tableType = $tableType['tableType'];
            $this->tableName = $tableType['tableName'];
        } else {
            $this->tableType = $tableType;
        }
        $this->tableLayout = new JobOfferTableLayout();
        $this->gridLayout = new JobOfferGridLayout();
    }

    /**
     * generateHtml
     * Generates desired offer box layout
     *
     * @return string
     */
    public function generateHtml()
    {

        $rows = [];
        // keeps start values for each display_bar_sum_#
        $displayBarSumValues = [];
        foreach ($this->fields as $key => $field) {

            $row = [
                'label' => $field['name'],
                'value' => $this->val($field),
                'opt' => ['type' => 'standard']
            ];
            if ($field['description'] === 'option_field') {
                $row['opt'] = [
                    'type' => 'option_field'
                ];
            } elseif ($field['description'] === 'stars_field') {
                $row['opt'] = [
                    'type' => 'stars_field',
                ];
            } elseif ($field['description'] === 'operating_systems_list') {
                $row['opt'] = [
                    'type' => 'operating_systems_list',
                ];
            } elseif ($field['description'] === 'option_with_description_field') {
                $fieldWords = explode('_', $field['slug']);
                if ($fieldWords[0] === 'desc') {
                    continue;
                }
                $fieldDesc = $this->val( find_field($this->fields, 'desc_' . $field['slug']) );
                $row['opt'] = [
                    'type' => 'option_with_description_field',
                ];
                $row['description'] = $fieldDesc;
            } elseif ($field['description'] === 'display_bar') {
                $row['opt'] = [
                    'type' => 'display_bar'
                ];

            } elseif (substr($field['description'], 0, strlen('display_bar_sum')) === 'display_bar_sum') {
                if (!isset($displayBarSumValues[$field['description']])) {
                    $displayBarSumValues[$field['description']] = 0;
                }
                $row['opt'] = [
                    'type' => 'display_bar_sum',
                    'start' => $displayBarSumValues[$field['description']]
                ];
                $displayBarSumValues[$field['description']] += $row['value'];
            }

            $rows[] = $row;
        }

        if ($this->tableType === 'standard_grid') {
            return $this->generateStandardGrid($rows);
        } elseif ($this->tableType === 'inside_grid') {
            return $this->generateSmallHeaderGrid($rows);
        } elseif ($this->tableType === 'options_with_description') {
            return $this->generateOptionWithDescriptionTable($rows);
        } elseif ($this->tableType === 'description_only') {
            return $this->generateDescriptionOnly($rows);
        }
        else {
            return $this->generateStandardTable($rows);
        }
    }

    /**
     * generateStandardTable
     * Generates table layout for default teble
     *
     * @return string
     */
    private function generateStandardGrid($rows)
    {

        $html = '';
        foreach ($rows as $row) {
            if($row['opt']['type'] === 'option_field') {
                $html .= $this->gridLayout->itemOptionMd4($row['label'], $row['value']);
            } elseif ($row['opt']['type'] === 'stars_field') {
                $html .= $this->gridLayout->itemStarsMd4($row['label'], $row['value']);
            } elseif ($row['opt']['type'] === 'standard') {
                $html .= $this->gridLayout->itemXs12($row['label'], $row['value']);
            }
        }

        if($html!=='') {
            $html = $this->gridLayout->header( $this->fieldGroup['name'] ) . $html;
            $html .= $this->gridLayout->footer();
        }

        return $html;
    }

    /**
     * generateStandardTable
     * Generates table layout for default teble
     *
     * @return string
     */
    private function generateSmallHeaderGrid($rows)
    {

        if( !empty($this->tableName) ) {
            $tableName = $this->tableName;
        } else {
            $tableName = $this->fieldGroup['name'];
        }

        $html = '';
        $html .= $this->gridLayout->smallHeader( $tableName );

        foreach ($rows as $row) {
            if ($row['opt']['type'] === 'stars_field') {
                $html .= $this->gridLayout->itemStarsXs12($row['label'], $row['value']);
            }
        }
        $html .= $this->gridLayout->footer();

        return $html;
    }

    /**
     * generateOptionWithDescriptionTable
     * Generates table layout for teble type options_with_description
     *
     * @return string
     */
    private function generateOptionWithDescriptionTable($rows)
    {

        $html = '';
        $html .= $this->tableLayout->header( $this->fieldGroup['name'] );

        foreach ($rows as $row) {
            $html .= $this->tableLayout->tr(
                $this->tableLayout->th($row['label']) .
                $this->tableLayout->tdWidthOption($row['value'], $row['description'])
            );
        }

        $html .= $this->tableLayout->footer();

        return $html;
    }

    /**
     * generateStandardTable
     * Generates table layout for default teble
     *
     * @return string
     */
    private function generateStandardTable($rows)
    {

        $html = '';
        $html .= $this->tableLayout->header( $this->fieldGroup['name'] );

        foreach ($rows as $row) {
            $trHtml = '';
            $trHtml .= $this->tableLayout->th($row['label']);

            if ($row['opt']['type'] === 'standard') {
                $trHtml .= $this->tableLayout->td($row['value']);
            } elseif ($row['opt']['type'] === 'operating_systems_list') {
                $trHtml .= $this->tableLayout->tdOperatingSystems($row['value']);
            } elseif ($row['opt']['type'] === 'display_bar') {
                $trHtml .= $this->tableLayout->tdDisplayBar($row['value']);
            } elseif ($row['opt']['type'] === 'display_bar_sum') {
                $trHtml .= $this->tableLayout->tdDisplayBarSum($row['value'], $row['opt']['start']);
            }

            $html .= $this->tableLayout->tr($trHtml);
        }

        $html .= $this->tableLayout->footer();

        return $html;
    }
    
    /**
     * generateDescriptionOnly
     * Generates one-celled table with header and the content of a 'description' field
     *
     * @return string
     */
    private function generateDescriptionOnly($rows)
    {

        $html = '';
        $html .= $this->tableLayout->header( $this->fieldGroup['name'] );

        foreach ($rows as $row) {
            if (strtolower($row['label']) == 'description') {
                $trHtml = $this->tableLayout->td($row['value']);
                $html .= $this->tableLayout->tr($trHtml);                
            }
        }

        $html .= $this->tableLayout->footer();

        return $html;
    }

    /**
     * val
     * helper for getting field value
     *
     * @param mixed
     * @return mixed
     */
    private function val($field)
    {
        return trim(field_value($field));
    }

}