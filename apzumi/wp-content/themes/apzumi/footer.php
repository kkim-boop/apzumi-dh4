        <div id="footer" class="container-fluid">
            <div id="footer-logo" class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img class="logo-img" src="<?php echo apzumi_image_directory();?>/footer-logo.png"></a>
                </div>
            </div>
            <div id="footer-areas" class="row">
                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                    <?php if ( is_active_sidebar( 'footer_area_1' ) ) : ?>
                        <?php dynamic_sidebar( 'footer_area_1' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                    <?php if ( is_active_sidebar( 'footer_area_2' ) ) : ?>
                        <?php dynamic_sidebar( 'footer_area_2' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                    <?php if ( is_active_sidebar( 'footer_area_3' ) ) : ?>
                        <?php dynamic_sidebar( 'footer_area_3' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                    <?php if ( is_active_sidebar( 'footer_area_4' ) ) : ?>
                        <?php dynamic_sidebar( 'footer_area_4' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                    <?php if ( is_active_sidebar( 'footer_area_5' ) ) : ?>
                        <?php dynamic_sidebar( 'footer_area_5' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                    <?php if ( is_active_sidebar( 'footer_area_6' ) ) : ?>
                        <?php dynamic_sidebar( 'footer_area_6' ); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div id="footer-footnotes" class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="footnotes">
                    <?php if ( is_active_sidebar( 'footnotes' ) ) : ?>
                        <?php dynamic_sidebar( 'footnotes' ); ?>
                    <?php endif; ?>
                    </div>
                    <?php if (function_exists('get_custom')): ?>
                    <div id="copyright-notice">
                        <?php echo get_custom('copyright_notice'); ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>  
        <?php if(!array_key_exists('visited', $_COOKIE) && function_exists('get_custom')): ?>
            <div id="cookie-info">
                <div class="container flex space-between">
                    <div>
                        <?php echo get_custom('cookie_message_info'); ?>
                    </div>
                    <a class="button" href="#" id="cookie-info-confirm"><?php echo get_custom('cookie_message_button'); ?></a>
                </div>
            </div>
        <?php endif; ?>
        <script>
            var themeUrl = '<?php echo get_template_directory_uri(); ?>'
        </script>

        <?php get_template_part('template-parts/page','privacy-policy')?>
        <?php wp_footer(); ?>
        
        <!-- Start of Async HubSpot Analytics Code -->
        <script type="text/javascript">
          (function(d,s,i,r) {
            if (d.getElementById(i)){return;}
            var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
            n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2140432.js';
            e.parentNode.insertBefore(n, e);
          })(document,"script","hs-analytics",300000);
        </script>
        <!-- End of Async HubSpot Analytics Code -->
    </body>
</html>