<?php get_header();?>
    <div id="blog-posts" class="section blog-posts-bg">
        <?php
            $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'nopaging' => true
            );
            $query = new WP_Query($args);
            while ($query->have_posts()) :
                $query->the_post();
                $categories = get_the_category();
                $category = (count($categories) > 0) ? $categories[0]->name : '';
                
        ?>
        <a class="post flex align-center justify-center" href="<?php the_permalink();?>"
                <?php if (has_post_thumbnail()):?> data-featured-image="<?php echo the_post_thumbnail_url('full');?>"<?php endif;?>>
            <div class="post-container">
                <h2><?php echo $category?></h2>
                <h1><?php the_title();?></h1>
                <div class="footer"><span class="date"><?php echo get_the_date('d M, Y');?></span> | <span class="author">by <?php the_author();?></span></div>
            </div>
        </a>
        <?php
            endwhile;
        ?>
    </div>
<?php
get_footer();
