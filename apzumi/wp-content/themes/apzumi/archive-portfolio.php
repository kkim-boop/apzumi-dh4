<?php get_header();?>
    <div id="our-work" class="slogan section our-work-bg" data-offset="0">
        <?php
        $realisations = array(
            'meedy' => array(
                'name' => 'Meedy',
                'description' => 'Comprehensive solution for health & insurance companies and their customers'
            ),
            'medatex' => array(
                'name' => 'Medatex',
                'description' => 'A tool to manage and simplify injury claims processes'
            ),
            'iscribes' => array(
                'name' => 'iScribes',
                'description' => 'A web and mobile tool that helps physicians focus on patients'
            ),
            'headmatters' => array(
                'name' => 'Headmatters',
                'description' => 'Essential tools to support mental health caregivers'
            )
        );

        foreach ($realisations as $realisationId => $realisation) {
            ?>
            <a class="realisation-hero flex align-center justify-start" data-realisation="<?php echo $realisationId;?>" href="/<?php echo $realisationId;?>">
                <div class="realisation-header">
                    <h1><?php echo $realisation['name']; ?></h1>
                    <h2><?php echo $realisation['description']; ?></h2>
                </div>
            </a>
        <?php } ?>
    </div>
    <a class="full-width-button button button-red no-fixed" href="/contact">
        <span>LET'S TALK</span>
        <span class="right-arrow"></span>
    </a>
<?php
get_footer();
