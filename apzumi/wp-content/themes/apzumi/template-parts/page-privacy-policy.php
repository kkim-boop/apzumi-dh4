<?php
$privacyPolicy = get_page_by_path( 'privacy-policy', OBJECT, 'page' );
?>
<div class="modal fade bs-apzumi-modal bs-privacy-modal" tabindex="-1" role="dialog" aria-labelledby="privacy-policy-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header flex">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?php echo apzumi_image_directory() ?>/close.svg" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <?php echo apply_filters('the_content', $privacyPolicy->post_content); ?>
            </div>
        </div>
    </div>
</div>