<div class="modal fade bs-apzumi-modal bs-apply-modal" tabindex="-1" role="dialog" aria-labelledby="apply-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header flex">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?php echo apzumi_image_directory() ?>/close.svg" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Send us your CV</h1>
                        <form id="applyForm" method="post" action="<?php echo get_template_directory_uri(); ?>/mail/apply.php" enctype="multipart/form-data">
                            <input type="text" placeholder="Full name" name="name">

                            <input type="text" placeholder="E-mail address" name="email">

                            <div class="clear"></div>
                            <span id="cv-file-btn" class="choose-file-btn">Choose file</span>
                            <input id="cv-file-txt" type="text" class="choose-file-input" readonly placeholder="CV">
                            <input id="cv-file" type="file" name="uploaded_cv" style="display:none;">

                            <div class="recaptcha-container flex center">
                                <div class="recaptcha-error-container">
                                    <div class="g-recaptcha" data-sitekey="6LdX7QoUAAAAAMsC6bm_KHDap-WYzuaMEsUAiOC5"></div>
                                </div>
                            </div>
                            <input type="hidden" name="offer" value="<?php echo(the_title()); ?>">
                            <button class="offer-btn">
                                <span class="offer-btn-text">SEND</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>