<?php get_header();?>
<div id="about">

    <div id="about-apzumi" class="slogan section flex" data-offset="0"<?php if (has_post_thumbnail()):?> style="background-image: url('<?php echo the_post_thumbnail_url('full');?>')"<?php endif;?>>
        <div class="container padded">
            <h1>Apzumi. Team of over 40 people that makes apps come true.</h1>
        </div>
        <img src="<?php echo apzumi_image_directory()?>/arrow-down.png" class="arrow-down">
    </div>

    <div id="about-lukasz" class="section about-person" data-offset="1">
        <div class="row">
            <div class="img-container left about-lukasz-bg col-xs-12 col-md-6"></div>
            <div class="desc-container col-xs-12 col-md-6">
                <div class="person-description-container flex">
                    <div class="person-description right">
                        <h1>Łukasz</h1>
                        <h2>CEO</h2>
                        <p>
                            <span class="about-label">Skills:</span> team building, business development, solving complex problems, high level IT strategy
                        </p>
                        <p>
                            <span class="about-label">Experience:</span> Over 6 years in leading fast growing IT company, over 15 years of professional IT experience
                        </p>
                        <p>
                            <span class="about-label">Specialties:</span> Industry 4.0, Digital Health, Artificial Intelligence, Augmented Reality 
                        </p>
                        <p>
                            <span class="about-label">Achievements:</span> building Apzumi from 1 to over 50 employees in 5 years
                        </p>
                        <p>
                            <span class="about-label">Hobbies:</span> Apzumi
                        </p>
                        <p>
                            <span class="about-label">Other say:</span> visionary and innovator, daily generator of countless ideas, maestro of both technology and business
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="about-asia" class="section about-person" data-offset="2">
        <div class="row">
            <div class="img-container right about-asia-bg col-xs-12 col-md-6 col-md-push-6"></div>
            <div class="desc-container col-xs-12 col-md-6 col-md-pull-6">
                <div class="person-description-container flex">
                    <div class="person-description left">
                        <h1>Asia</h1>
                        <h2>Project Manager</h2>
                        <p>
                            <span class="about-label">Skills:</span> conducting multiple projects at the same time, flawless coordination of all aspects of projects, outstanding team motivator
                        </p>
                        <p>
                            <span class="about-label">Experience:</span> scientist and owner of a bioinformatics company, over 5 years; project manager, over 5 years
                        </p>
                        <p>
                            <span class="about-label">Specialties:</span> Medical Industry, Data Security, EHR Systems
                        </p>
                        <p>
                            <span class="about-label">Achievements:</span> earned PhD degree at age of 30 and conducted 10 successful projects in Apzumi within one year
                        </p>
                        <p>
                            <span class="about-label">Personal notes:</span> spends all her free time with her children
                        </p>
                        <p>
                            <span class="about-label">Hobbies:</span> promoting and teaching Python programming language to women
                        </p>
                        <p>
                            <span class="about-label">Other say:</span> Apzumi's smile generator, Hero PM, takes care
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="about-mikolaj" class="section about-person" data-offset="3">
        <div class="row">
            <div class="img-container left about-mikolaj-bg col-xs-12 col-md-6"></div>
            <div class="desc-container col-xs-12 col-md-6">
                <div class="person-description-container flex">
                    <div class="person-description right">
                        <h1>Mikołaj</h1>
                        <h2>Project Manager</h2>
                        <p>
                            <span class="about-label">Skills:</span> managing multiple projects from different industries in Agile approach, motivating members of the team, programming and data structure modeling
                        </p>
                        <p>
                            <span class="about-label">Experience:</span> Java and J2EE Programmer, 5 years; Project Manager, 2 Years
                        </p>
                        <p>
                            <span class="about-label">Specialties:</span> Insurance, Medical and Advertising Industries, and system that support sales, time and task management
                        </p>
                        <p>
                            <span class="about-label">Achievements:</span> independently conducted two midsize projects - one in the Construction Industry and one in the TV Industry
                        </p>
                        <p>
                            <span class="about-label">Personal notes:</span> happy husband and father
                        </p>
                        <p>
                            <span class="about-label">Hobbies:</span> carpentry, volleyball, football
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--     <div id="about-karol" class="section about-person" data-offset="4">
        <div class="row">
            <div class="img-container right about-karol-bg col-xs-12 col-md-6 col-md-push-6"></div>
            <div class="desc-container col-xs-12 col-md-6 col-md-pull-6">
                <div class="person-description-container flex">
                    <div class="person-description left">
                        <h1>Karol</h1>
                        <h2>Project Manager & Java Developer</h2>
                        <p>
                            <span class="about-label">Skills:</span> Java, ActionScript/Flex, SQL, Javascript, Hibernate, Spring, JMS
                        </p>
                        <p>
                            <span class="about-label">Experience:</span> Java developer, 5 years; Java Web Developer, 3 years; Project Leader, 1 year
                        </p>
                        <p>
                            <span class="about-label">Specialties:</span> Insurance & Healthcare Industries
                        </p>
                        <p>
                            <span class="about-label">Achievements:</span> Project Leader of Meedy, which won the Gazeta Bankowa technology contest "2015 Leader" in the category of "Medicine and Pharmacy"
                        </p>
                        <p>
                            <span class="about-label">Personal notes:</span> favorite quote: "Practice, practice, practice"
                        </p>
                        <p>
                            <span class="about-label">Hobbies:</span> photomontage, audio and video production, music composition, space exploration
                        </p>
                        <p>
                            <span class="about-label">Other say:</span> very friendly and helpful, enjoys sharing his knowledge
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div id="values" class="section flex">
        <div class="container padded section-content">
            <h1>Our core <span class="nobr">values guide</span> us in all we do.</h1>
            <div class="col first">
                <h1>Partnership</h1>
                <h2>
                    <span class="red">Your success is our success.</span><br> Honesty, commitment and the ability to take mutual risk.
                </h2>
                <h1>Passion</h1>
                <h2>
                    You deserve nothing less than the highest quality. Our passion delivers the best results and leaves no room for cost-cutting.
                </h2>
            </div>
            <div class="col">
                <h1>Progress</h1>
                <h2>Your growth is proactively developed when we take the initiative in suggesting creative ideas to move your business ahead.
                    Your R&D is safe in our hands, where our employees continuously develop their professional and personal development.</h2>
            </div>
        </div>
    </div>

    <a class="full-width-button button button-red no-fixed text-uppercase about-us-full-width-button" href="/contact">
        <span>We all would like to meet you, visit our office and meet the rest</span>
        <span class="right-arrow"></span>
    </a>
</div>
<?php
get_footer();
