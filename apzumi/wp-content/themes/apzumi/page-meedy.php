<?php
get_header();

$realisation = array(
    'id' => 'meedy',
    'name' => 'Meedy',
    'slogan' => 'Comprehensive solution for health & insurance companies and their customers',
    'nImages' => 4,
    'references' =>  array(
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        ),
        array(
            'text' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco ea commodo consequat. 
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
            'authorName' => "Jon Smith",
            'authorImage' => "img/referers/anonymous_icon.png?imgv=1.2",
            'authorPosition' => "Recommendation Author"
        )
    ),
    'technologies' => array("angularjs", "java", "tomcat", "mysql", "hibernate", "spring", "flex"),
    'description' => array(
        'One of our flagship projects, it started with a grand vision and is '
        . 'being achieved step by step. Meedy’s vision is to improve the domestic health insurance industry. '
        . 'By joining the efforts of insurance companies, medical entities and individual physicians, '
        . 'the system put in place makes the process of injury claims the most effective so far. '
        . 'Such a large-scale project that would change the quality of life for '
        . 'people was definitely our kind of project!<br><br>Finding the vision of the product was easy. '
        . 'The challenging part was managing the project in such a dynamic surrounding where nobody '
        . 'before had implemented this kind of solution. We fulfilled all immediate needs that appeared, '
        . 'allowing the system to be implemented with full efficiency and no delay.<br><br>',

        'Daily meetings with Meedy and the '
        . 'agile work of the highly skilled developers resulted in a system far superior to any other on the market.<br><br>'
        . 'Today, the system is used by 750 medical institutions and over 600,000 visits have '
        . 'been organized through the system. Implementation of the system was an important step in Meedy’s performance, '
        . 'as it brought a boost in revenue that continues today. The organizational flow between insurance companies, '
        . 'medical entities, doctors and the patients, put in place thanks to the system, saves a lot of time and money '
        . 'for all users. We are currently developing new usability of the system and preparing it for use by at least 4 million users.'
    )
);
?>

<?php
$technologyIcons = array(
    'activiti' => 'activity.png?imgv=1.2',
    'android' => 'android.svg',
    'angularjs' => 'angularjs.png?imgv=1.2',
    'css3' => 'css3.png?imgv=1.2',
    'flex' => 'flex.png?imgv=1.2',
    'hibernate' => 'hibernate.png?imgv=1.2',
    'html5' => 'html5.png?imgv=1.2',
    'ios' => 'ios.svg',
    'java' => 'java.png?imgv=1.2',
    'less' => 'less.png?imgv=1.2',
    'mysql' => 'mysql.svg',
    'nodejs' => 'nodejs.png?imgv=1.2',
    'php' => 'php.png?imgv=1.2',
    'sass' => 'sass.png?imgv=1.2',
    'spring' => 'spring.png?imgv=1.2',
    'tomcat' => 'tomcat.svg'
);

if($realisation) {
    ?>
    <div id="realisation-page">
        <div class="slogan section flex realisation-hero align-center small-align-start justify-start" id="<?php echo $realisation['id'];?>">
            <div id="our-work-link">
                <a href="/portfolio">
                    <span></span>
                </a>
                <h3>
                    Portfolio
                </h3>
            </div>
            <div class="realisation-wrapper">
                <div class="realisation-header">
                    <h1><?php echo $realisation['name']; ?></h1>
                    <h2><?php echo $realisation['slogan']; ?></h2>
                </div>
            </div>
        </div>
        <div id="images" class="realisation-images section">
            <div id="next">
                <img src="<?php echo apzumi_image_directory()?>/next.png?imgv=1.2" class="arrow-next">
            </div>
            <div id="previous">
                <img src="<?php echo apzumi_image_directory()?>/previous.png?imgv=1.2" class="arrow-previous">
            </div>
            <div class="realisation-image-wrapper flex">
                <?php
                for ($i = $realisation['nImages'] - 1; $i >= 0 ; $i--) {
                    ?>
                    <div class="realisation-image" data-realisation="<?php echo $realisation['id'];?>" data-index="<?php echo $i;?>">
                        <img src="<?php echo apzumi_image_directory()?>/<?php echo $realisation['id'];?>_<?php echo $i;?>.png?imgv=1.2">
                    </div>
                    <?php
                }
                ?>
                <div class="indicator">
                    <?php
                    for ($i = 0; $i < $realisation['nImages']; $i++) {
                        ?>
                        <a class="section-link" data-index="<?php echo $i; ?>">
                            <div class="link-point"></div>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div id="stack-reference" class="section">
            <div id="technology-stack">
                <h3>
                    TECHNOLOGY STACK
                </h3>
                <div id="technologies" class="flex space-between small-vertical small-justify-center">
                    <?php
                    foreach ($realisation['technologies'] as $technology) {
                        if (array_key_exists($technology, $technologyIcons)) {
                            ?>
                            <img src="<?php echo apzumi_image_directory()?>/technologies/<?php echo $technologyIcons[$technology];?>" data-technology="<?php echo $technology; ?>">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="realisation-description" class="flex">
                <?php if(count($realisation['description']) >= 2) { ?>
                    <div class="container padded default-width">
                        <div class="col">
                            <?php echo $realisation['description'][0]; ?>
                        </div>
                        <div class="col">
                            <?php echo $realisation['description'][1]; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <a class="full-width-button button button-blue no-fixed" href="/portfolio">
            <span class="left-arrow"></span>
            <span>BACK TO PORTFOLIO</span>
        </a>
        <a class="full-width-button button button-red no-fixed" href="/contact">
            <span>LET'S TALK</span>
            <span class="right-arrow"></span>
        </a>
    </div>
<?php }

get_footer();
