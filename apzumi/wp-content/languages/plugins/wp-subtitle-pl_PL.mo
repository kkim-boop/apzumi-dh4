��          �            h     i  )   r  X   �     �          '     ;     T     q     z  =   �     �  )   �  '   �  ^  '     �  ,   �  l   �     (     A     \  %   p     �  	   �     �  9   �       ,     '   G     	                                      
                                     %s Field Add subtitle field to product edit screen Adds a subtitle field to pages and posts. Possible to add support for custom post types. Ben Huson, Husani Oakley Enable Product Subtitles Enter subtitle here Show on product archives Show on single product pages Subtitle Subtitle Display This field has moved from a meta box to below the post title. WP Subtitle http://wordpress.org/plugins/wp-subtitle/ https://github.com/benhuson/wp-subtitle PO-Revision-Date: 2018-09-09 16:37:32+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: pl
Project-Id-Version: Plugins - WP Subtitle - Stable (latest release)
 Pole %s Dodaje podtytuł na ekranie edycji produktu. Dodaje pole podtytułu dla stron oraz wpisów. Możliwe włączenie podtytułów w różnych typach treści. Ben Huson, Husani Oakley Włącz podtytuł produktu Wprowadź podtytuł Pokaż na stronie archiwum produktów Pokaż na stronie produktu Podtytuł Wyświetlanie podtytułu Pole zostało przeniesione z mata boksa pod tytuł wpisu. WP Subtitle http://pl.wordpress.org/plugins/wp-subtitle/ https://github.com/benhuson/wp-subtitle 