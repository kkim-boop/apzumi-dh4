// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class ProjectSlide extends Component {

  static slug = 'apzumi_project_slide';

  render() {
    const ImageSrc = this.props.image_src;
    const Content = this.props.content;

    return (
        <div>          
          <img className="project-image" alt="" src={ImageSrc}></img>
          <div className="project-description">
            <Content></Content>
          </div>
        </div>
    );
  }
}

export default ProjectSlide;
