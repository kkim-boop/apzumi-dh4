<?php

class APZUMI_TestimonialSlider extends ET_Builder_Module {

  public $slug       = 'apzumi_testimonial_slider';
  public $child_slug = 'apzumi_testimonial_slide';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'Andrzej Krajna',
		'author_uri' => '',
	);

	public function init() {
		$this->name = esc_html__( 'Apzumi Testimonial Slider', 'apzumi-apzumi-divi' );
	}

	public function get_fields() {
		return array(
			'title' => array(
				'label'           => esc_html__( 'Title', 'dicm-divi-custom-modules' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as title.', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'main_content',
			),
			'id' => array(
				'label'           => esc_html__( 'ID', 'dicm-divi-custom-modules' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will become slider ID', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'main_content',
      ),
			'testimonial_count' => array(
				'label'           => esc_html__( 'Liczba rekomendacji', 'dicm-divi-custom-modules' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will become slider ID', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'main_content',
			)
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {

    	$section_links = "";
    	for ($i = 0; $i < $this->props['testimonial_count']; $i++) {
			$section_links .=  sprintf(
				'<a class="section-link" data-index="%1$s">
				<div class="link-point"></div>
				</a>', $i);
		}

		return sprintf(
			'<div id="%3$s" class="apzumi_slider flex align-start no-fixed">
				%2$s
				<div class="indicator-container">
					<div class="content-area flex align-end justify-center">
							<div class="indicator">
								%4$s
							</div>
					</div>
				</div>
			</div>',
			esc_html( $this->props['title'] ),
			et_sanitized_previously( $this->content ),
			$this->props['id'],
			$section_links
		);
	}
}

new APZUMI_TestimonialSlider;
