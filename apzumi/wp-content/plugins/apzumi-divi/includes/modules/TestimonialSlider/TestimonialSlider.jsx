// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class TestimonialSlider extends Component {

  static slug = 'apzumi_testimonial_slider';

  render() {
    const Id = this.props.id;
    const Content = this.props.content;

    return (
			<div id={Id} className="apzumi_slider flex align-start no-fixed">{Content}</div>
    );
  }
}

export default TestimonialSlider;
