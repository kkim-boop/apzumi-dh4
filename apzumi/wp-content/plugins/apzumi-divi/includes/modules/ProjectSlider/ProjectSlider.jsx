// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class ProjectSlider extends Component {

  static slug = 'apzumi_project_slider';

  render() {
    const Id = this.props.id;
    const Content = this.props.content;

    return (
			<div id={Id} className="apzumi_slider">
        <div className="image-overlay-container">
          <div className="image-overlay-left"></div>
          <div className="image-overlay-right"></div>
        </div>
        <div className="description-overlay-container">          
          <div className="description-overlay-left"></div>
          <div className="description-overlay-right"></div>
        </div>
        {Content}
      </div>
    );
  }
}

export default ProjectSlider;
