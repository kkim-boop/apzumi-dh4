<?php

class APZUMI_ProjectSlider extends ET_Builder_Module {

  	public $slug       = 'apzumi_project_slider';
  	public $child_slug = 'apzumi_project_slide';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'Andrzej Krajna',
		'author_uri' => '',
	);

	public function init() {
		$this->name = esc_html__( 'Apzumi Project Slider', 'apzumi-apzumi-divi' );
	}

	public function get_fields() {
		return array(
			'id' => array(
				'label'           => esc_html__( 'ID', 'dicm-divi-custom-modules' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will become slider ID', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'main_content',
      ),
			'slide_count' => array(
				'label'           => esc_html__( 'Liczba slajdów', 'dicm-divi-custom-modules' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will become slider ID', 'dicm-divi-custom-modules' ),
				'toggle_slug'     => 'main_content',
			)
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {

    	$section_links = "";
    	for ($i = 0; $i < $this->props['slide_count']; $i++) {
			$section_links .=  sprintf(
				'<a class="section-link" data-index="%1$s">
				<div class="link-point"></div>
				</a>', $i);
		}

		return sprintf(
			'<div id="%3$s" class="apzumi_slider flex no-fixed">
			<a id="previous" class="arrow-previous"><img src="https://www.kimkstar.com/apzumi/wp-content/uploads/2020/03/Big-chevron.svg">Last Project</a>
				<a id="next" class="arrow-next"><img src="https://www.kimkstar.com/apzumi/wp-content/uploads/2020/03/Big-chevron2.svg">Next Project</a>
				<div class="apzumi_slider" >
                <div class="image-overlay-container">
					<div class="image-overlay-left"></div>
					<div class="image-overlay-right"></div>
				</div>
                </div>
				<div class="description-overlay-container">          
					<div class="description-overlay-left"></div>
					<div class="description-overlay-right"></div>
				</div>
				%2$s
				<div class="indicator-container">
					<div class="content-area flex align-end justify-center">
						<div class="indicator">
							%4$s
						</div>
					</div>
				</div>
			</div>',
			esc_html( $this->props['title'] ),
			et_sanitized_previously( $this->content ),
			$this->props['id'],
			$section_links
		);
	}
}

new APZUMI_ProjectSlider;