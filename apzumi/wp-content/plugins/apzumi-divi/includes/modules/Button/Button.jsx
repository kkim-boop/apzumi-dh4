// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class Button extends Component {

  static slug = 'apzumi_button';

  render() {
    const LinkText = this.props.link_text;
    const LinkUrl = this.props.link_url;

    return (
        <a href={LinkUrl} className="link-container">
          <span className="link-text">{LinkText}</span>
        </a>
    );
  }
}

export default Button;
