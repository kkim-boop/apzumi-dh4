// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class SpecialtyHeading extends Component {

  static slug = 'apzumi_specialty_heading';

  render() {
    const ImageSrc = this.props.image_src;
    const Content = this.props.content;

    return (
        <div>          
          <img className="specialty-image" alt="" src={ImageSrc}></img>
          <div className="specialty-heading-text">
            <Content></Content>
          </div>
        </div>
    );
  }
}

export default SpecialtyHeading;
