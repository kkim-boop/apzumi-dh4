import HelloWorld from './HelloWorld/HelloWorld';
import PageLink from './PageLink/PageLink';
import Button from './Button/Button';
import TestimonialSlider from './TestimonialSlider/TestimonialSlider';
import TestimonialSlide from './TestimonialSlide/TestimonialSlide';
import ProjectSlider from './ProjectSlider/ProjectSlider';
import ProjectSlide from './ProjectSlide/ProjectSlide';
import SpecialtyHeading from './SpecialtyHeading/SpecialtyHeading';

export default [
    HelloWorld,
    PageLink,
    Button,
    TestimonialSlider,
    TestimonialSlide,
    ProjectSlide,
    ProjectSlider,
    SpecialtyHeading
];
