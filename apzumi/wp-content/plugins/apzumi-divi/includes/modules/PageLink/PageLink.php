<?php

class APZUMI_PageLink extends ET_Builder_Module {

	public $slug       = 'apzumi_page_link';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'Andrzej Krajna',
		'author_uri' => '',
	);

	public function init() {
		$this->name = esc_html__( 'Apzumi Link', 'apzumi-apzumi-divi' );
	}

	public function get_fields() {
		return array(
			'link_text' => array(
				'label'           => esc_html__( 'Tekst', 'apzumi-apzumi-divi' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Wyświetlany tekst linku', 'apzumi-apzumi-divi' ),
				'toggle_slug'     => 'main_content',
			),
			
			'link_url' => array(
				'label'           => esc_html__( 'URL', 'apzumi-apzumi-divi' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Adres linku', 'apzumi-apzumi-divi' ),
				'toggle_slug'     => 'main_content',
			)
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
		return sprintf(
			'<a href="%2$s" target="_blank" class="link-container"><span class="link-text">%1$s</span></a>',
			$this->props['link_text'],
			$this->props['link_url']
		);
	}
}

new APZUMI_PageLink;
