<?php

class APZUMI_TestimonialSlide extends ET_Builder_Module {

  public $slug       = 'apzumi_testimonial_slide';
  public $type                     = 'child';
	public $child_title_var          = 'title';
	public $child_title_fallback_var = 'subtitle';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'Andrzej Krajna',
		'author_uri' => '',
	);

	public function init() {
		$this->name = esc_html__( 'Apzumi Testimonial Slide', 'apzumi-apzumi-divi' );
	}

	public function get_fields() {
		return array(
			'content' => array(
				'label'           => esc_html__( 'Content', 'apzumi-apzumi-divi' ),
				'type'            => 'tiny_mce',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Content entered here will appear inside the module.', 'apzumi-apzumi-divi' ),
				'toggle_slug'     => 'main_content',
			)
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
		return sprintf( 
			'%1$s',
			$this->props['content']
		);
	}
}

new APZUMI_TestimonialSlide;
