// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class TestimonialSlide extends Component {

  static slug = 'apzumi_testimonial_slide';

  render() {
    const Content = this.props.content;

    return (
        <Content></Content>
    );
  }
}

export default TestimonialSlide;
